<?php
declare(strict_types=1);

namespace Richbuilds\Tests;

use PDO;
use PHPUnit\Framework\TestCase;
use Richbuilds\Orm\Driver\Driver;
use Richbuilds\Orm\Driver\DriverException;
use Richbuilds\Orm\Driver\MySql\MySqlDriver;
use Richbuilds\Orm\Helper\DataTypeException;
use Richbuilds\Orm\Model;
use Richbuilds\Orm\ModelArray;
use Richbuilds\Orm\ModelException;
use Richbuilds\Orm\Query;

/**
 *
 */

/**
 *
 */
class ModelTest extends TestCase
{

    private static Driver|null $Driver = null;

    /**
     * @return void
     * @throws DriverException
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$Driver = new MySqlDriver(new PDO('mysql:host=127.0.0.1;dbname=orm_test', 'admin', 'x3r0xx3r0x'));
    }

    /**
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$Driver = null;
    }

    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetFieldInfoReturnsAllIfNoFieldNameSpecified(): void
    {
        $model = new Model(self::$Driver, 'posts');

        $field_info = $model->getFieldMeta();

        self::assertCount(7, $field_info);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetFieldInfoReturnsSingleFieldInfoIfFieldNameSpecified(): void
    {
        $model = new Model(self::$Driver, 'posts');

        $field_info = $model->getFieldMeta('id');

        self::assertTrue($field_info['pk']);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetFieldInfoFailsForInvalidFieldName(): void
    {
        $model = new Model(self::$Driver, 'posts');

        $this->expectErrorMessage('field invalid_field not found in table posts');

        $model->getFieldMeta('invalid_field');
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testFindByPkWorks(): void
    {
        $original = (new Model(self::$Driver, 'posts', [
            'title' => uniqid('title ', true)
        ]))
            ->save();

        $reload = (new Model(self::$Driver, 'posts'))
            ->fetchByPk($original->getPkValue());

        self::assertEquals($original->get('title'), $reload->get('title'));
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testFetchFirstByReturnsFalseIfNoMatches(): void
    {
        $model = (new Model(self::$Driver, 'posts'))
            ->fetchFirstBy(
                ['title' => 'unknown title']
            );

        self::assertFalse($model);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testFetchByPkFailsForTableWithNoPk(): void
    {
        $this->expectErrorMessage('table test_no_pk has no primary key');

        (new Model(self::$Driver, 'test_no_pk'))
            ->fetchByPk(1);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testFetchByPkFailsIfNotGivenCompositeKeyForTableWithCompositePrimaryKey(): void
    {
        $this->expectErrorMessage('composite primary key expects array');

        $model = (new Model(self::$Driver, 'test_composite_pk'))
            ->fetchByPk(1);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testFetchByCompositeFkWorks(): void
    {
        self::$Driver->truncate('test_composite_pk');

        $original = (new Model(self::$Driver, 'test_composite_pk', [
            'f1' => 1,
            'f2' => 1
        ]))
            ->save();

        $reload = (new Model(self::$Driver, 'test_composite_pk'))->fetchByPk([
            'f1' => 1,
            'f2' => 1
        ]);

        self::assertNotFalse($reload);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetValuesFailsWithInvalidFieldValue(): void
    {
        $this->expectErrorMessage('field id in table posts - invalid format - expected int');

        (new Model(self::$Driver, 'posts'))
            ->setValues([
                'id' => 'value'
            ]);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetFailsForInvalidFieldName(): void
    {
        $this->expectErrorMessage('unknown field invalid_field in table posts');

        (new Model(self::$Driver, 'posts'))
            ->set('invalid_field', 'value');

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetChildArrayWorks(): void
    {

        $model = (new Model(self::$Driver, 'posts', [
            'title' => uniqid('title ', true),
            'comments' => [
                [
                    'name' => uniqid('name ', true),
                    'comment' => uniqid('comment ', true)
                ]
            ]
        ]));

        self::assertIsArray($model->get('comments'));

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetFailsIfArrayPassedToFkField(): void
    {
        $this->expectErrorMessage('field id in table posts - invalid format - unexpected array');

        $model = (new Model(self::$Driver, 'posts'))
            ->set('id', ['foo' => 'bar']);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetWithParentArrayWorks(): void
    {
        $model = (new Model(self::$Driver, 'posts'))
            ->set('author_id', [
                'name' => uniqid('name ', true)
            ]);

        self::assertIsArray($model->get('author_id'));
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetHabtmFailsForInvalidHabtmTableName(): void
    {
        $this->expectErrorMessage('unknown habtm invalid_habtm in table posts');

        (new Model(self::$Driver, 'posts', [
            'invalid_habtm' => [
                'name' => uniqid('tag ', true)
            ]
        ]));
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetWithParentModelWorks(): void
    {
        $author = (new Model(self::$Driver, 'users', [
            'name' => uniqid('name ', true)
        ]))->save();

        $model = (new Model(self::$Driver, 'posts'))
            ->set('author_id', $author);

        self::assertIsInt($model->get('author_id'));
        self::assertInstanceOf(Model::class, $model->fetchParent('author_id'));
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetWithParentModelFailsForInvalidModel(): void
    {

        $tag = new Model(self::$Driver, 'tags');

        $this->expectErrorMessage('field author_id in table posts - invalid format - expected users');

        $model = (new Model(self::$Driver, 'posts'))
            ->set('author_id', $tag);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSaveWithoutRequiredFieldsFails(): void
    {

        $this->expectErrorMessage('field title in table posts is required');

        (new Model(self::$Driver, 'posts'))
            ->save();

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSaveWithNoPkWorks(): void
    {

        $model = (new Model(self::$Driver, 'test_no_pk', [
            'name' => uniqid('name ', true)
        ]))->save();

        self::assertIsString($model->get('name'));

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSaveWithUuidPkWorks(): void
    {

        $model = (new Model(self::$Driver, 'test_uuid_pk', [
            'name' => uniqid('name ', true),
            'parent_id' => [
                'name' => uniqid('name', true)
            ]
        ]))->save();

        self::assertIsString($model->getPkValue());

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSaveHabtmWorks(): void
    {

        $model = (new Model(self::$Driver, 'posts', [
            'title' => uniqid('title ', true),
            'tags' => [
                [
                    'name' => uniqid('name ', true)
                ],
                [
                    'name' => uniqid('name ', true)
                ],
                [
                    'name' => uniqid('name ', true)
                ],
            ]
        ]))->save();

        self::assertCount(3, $model->fetchChildren('tags'));

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testUpdateWorksForCompositePk(): void
    {

        $model = (new Model(self::$Driver, 'test_composite_pk', [
            'f1' => 1,
            'f2' => 1,
            'name' => uniqid('name ', true)
        ]))->save();

        $new_name = uniqid('name ', true);

        $model->set('name', $new_name)
            ->save();

        $reload = (new Model(self::$Driver, 'test_composite_pk'))
            ->fetchByPk($model->getPkValue());

        self::assertEquals($new_name, $reload->get('name'));
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testUpdateWorksForSimplePk(): void
    {

        $model = (new Model(self::$Driver, 'posts', [
            'title' => uniqid('title ', true),
        ]))->save();

        $new_title = uniqid('title ', true);

        $model->set('title', $new_title)
            ->save();

        $reload = (new Model(self::$Driver, 'posts'))
            ->fetchByPk($model->getPkValue());

        self::assertEquals($new_title, $reload->get('title'));
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSaveWithHasManyChildrenWorks(): void
    {
        $model = (new Model(self::$Driver, 'posts', [
            'title' => uniqid('title ', true),
            'comments' => [
                [
                    'name' => uniqid('name ', true),
                    'comment' => uniqid('comment ', true)
                ],
                [
                    'name' => uniqid('name ', true),
                    'comment' => uniqid('comment ', true)
                ],
                [
                    'name' => uniqid('name ', true),
                    'comment' => uniqid('comment ', true)
                ],
            ]
        ]))->save();

        $children = $model->fetchChildren('comments');

        self::assertCount(3, $children);

        foreach ($children as $child) {
            self::assertEquals('comments', $child->getTableName());
        }

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetValuesPopulatesMissingFieldsWithNull(): void
    {

        $values = (new Model(self::$Driver, 'posts'))
            ->getValues();

        self::assertEquals(
            [
                'id' => null,
                'title' => null,
                'author_id' => null,
                'created' => null,
                'updated' => null,
                'category_id' => null,
                'body' => null
            ],
            $values
        );

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetFailsForInvalidFieldName(): void
    {
        $this->expectErrorMessage('unknown field invalid_field in table posts');

        (new Model(self::$Driver, 'posts'))->get('invalid_field');
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetPKValueFailsForTableWithoutPk(): void
    {
        $this->expectErrorMessage('table test_no_pk has no primary key');

        (new Model(self::$Driver, 'test_no_pk'))
            ->getPkValue();
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testFetchParentFailsForInvalidParentName(): void
    {
        $this->expectErrorMessage('unknown parent invalid_field_name in table posts');

        $model = (new Model(self::$Driver, 'posts'))
            ->fetchParent('invalid_field_name');
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testFetchParentReturnsNullIfNotSet(): void
    {
        $author = (new Model(self::$Driver, 'posts'))
            ->fetchParent('author_id');

        self::assertNull($author);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetUniqueNameWorksRecursively(): void
    {
        $model = (new Model(self::$Driver, 'categories', [
            'name' => uniqid('name ', true),
            'parent_id' => [
                'name' => uniqid('name ', true),
            ]
        ]))->save();

        self::assertEquals(
            $model->get('name') . ' :: ' . $model->fetchParent()->get('name'),
            $model->getUniqueName(' :: ')
        );
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetUniqueKeyWorksForUniqueFields(): void
    {
        $model = (new Model(self::$Driver, 'posts', [
            'title' => uniqid('title ', true),
        ]))->save();

        self::assertEquals(
            $model->get('title'),
            $model->getUniqueName(' :: ')
        );

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testAutocompleteWorks(): void
    {

        self::$Driver->truncate('test_autocomplete');

        $models = new ModelArray(
            new Model(self::$Driver, 'test_autocomplete', ['name' => 'one']),
            new Model(self::$Driver, 'test_autocomplete', ['name' => 'two']),
            new Model(self::$Driver, 'test_autocomplete', ['name' => 'three']),
            new Model(self::$Driver, 'test_autocomplete', ['name' => 'four']),
            new Model(self::$Driver, 'test_autocomplete', ['name' => 'five']),
        );

        $models->save();

        $model = new Model(self::$Driver, 'test_autocomplete');

        self::assertCount(0, $model->autocomplete('xxx'));
        self::assertCount(1, $model->autocomplete('one'));
        self::assertCount(2, $model->autocomplete('t'));
        self::assertCount(5, $model->autocomplete(''));

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetUniqueKeyFieldNamesFailsForTableWithNoUniqueIndexes(): void
    {

        $this->expectErrorMessage('primary key or no unique key set for table test_no_pk');

        $model = (new Model(self::$Driver, 'test_no_pk'))
            ->getUniqueName();
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testGetUniqueKeyFieldNamesDefaultToPkIfNoUniqueIndexFound(): void
    {

        self::$Driver->truncate('test_truncate');

        $model = (new Model(self::$Driver, 'test_truncate', [
            'name' => 'foo'
        ]))->save();

        self::assertEquals($model->getPkValue(), $model->getUniqueName());
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSetIgnoresUnknownFields(): void
    {

        $model = new Model(self::$Driver, 'test_datatypes');

        $errors = $model->validate(['not_null' => '', 'foo' => 'bar']);

        self::assertCount(0, $errors);

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testDeleteWorks(): void
    {

        $title = uniqid('title ', true);

        $model = (new Model(self::$Driver, 'posts', [
            'title' => $title
        ]))
            ->save();

        $model->delete();

        $reload = $model->fetchFirstBy(['title' => $title]);

        self::assertFalse($reload);

    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testDeleteFailsIfNoPk(): void
    {
        $model = new Model(self::$Driver, 'posts');

        $this->expectErrorMessage('no primary key set - cannot delete');

        $model->delete();
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testDeleteWorksForCompositePks(): void
    {
        self::$Driver->truncate('test_composite_pk');

        $model = (new Model(self::$Driver, 'test_composite_pk', [
            'f1' => 1,
            'f2' => 1
        ]))->save();

        $model->delete();

        $reload = $model->fetchByPk([
            'f1' => 1,
            'f2' => 1
        ]);

        self::assertFalse($reload);
    }


    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testCascadingDeleteWorks(): void
    {
        $author = (new Model(self::$Driver, 'users', [
            'name' => uniqid('name ', true)
        ]))->save();

        $post = (new Model(self::$Driver, 'posts', [
            'title' => uniqid('title ', true),
            'author_id' => $author
        ]))->save();

        $author_pk = $author->getPkValue();
        $post_pk = $post->getPkValue();

        $author = $author->cascadingDelete();

        $reload_author = $author->fetchByPk($author_pk);
        self::assertFalse($reload_author);

        $reload_post = $post->fetchByPk($post_pk);
        self::assertFalse($reload_post);

        // note $post still contains a model with its pk set even though the row has been deleted
    }

    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function saveWithNoAutoPkWorks(): void
    {

        $model = (new Model(self::$Driver, 'no_auto_pk_test', [
                'id' => 1,
                'name' => uniqid('name ', true)
            ]
        ))->save();
    }

    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testNoAutoPkWorks(): void
    {

        $id = uniqid('id ', true);

        $model = (new Model(self::$Driver, 'test_no_auto_pk', [
            'id' => $id,
            'name' => 'foo'
        ]))->save();

        self::assertEquals($id, $model->getPkValue());
    }

    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSave(): void
    {


        $model = new Model(self::$Driver, 'categories');

        $errors = $model->validate([
            'name' => uniqid('name', true)
        ]);

        self::assertCount(0, $errors);

    }

    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testSaveCatchesDBLevelErrors(): void
    {

        /*
         * https://stackoverflow.com/questions/25844786/unique-multiple-columns-and-null-in-one-column
         *
         * null parent_id's are considered unique so we have to create a parent to test for uniqueness fail
         */

        $parent = (new Model(self::$Driver, 'categories', [
            'name' => uniqid('name', true)
        ]))->save();

        $name = uniqid('name', true);

        $model1 = (new Model(self::$Driver, 'categories', [
            'parent_id' => $parent,
            'name' => $name
        ]))->save();

        $this->expectErrorMessage('Integrity constraint violation');

        $model2 = (new Model(self::$Driver, 'categories', [
            'parent_id' => $parent,
            'name' => $name
        ]))->save();

        // assert we are no longer in a transaction
        self::assertFalse(self::$Driver->inTransaction());
    }

    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testTruncateWorks(): void
    {
        $model = (new Model(self::$Driver, 'test_truncate', [
            'name' => uniqid('name ', true)
        ]))
            ->save();

        self::$Driver->truncate('test_truncate');

        $fetch = (new Query(self::$Driver, 'test_truncate'))->fetch();

        self::assertEmpty($fetch);

    }

}
