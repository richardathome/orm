<?php
declare(strict_types=1);

namespace Richbuilds\Tests\Driver;

use DateTime;
use Exception;
use PDO;
use PHPUnit\Framework\TestCase;
use Richbuilds\Orm\Driver\Driver;
use Richbuilds\Orm\Driver\DriverException;
use Richbuilds\Orm\Driver\MySql\MySqlDriver;
use Richbuilds\Orm\Helper\DataTypeException;
use Richbuilds\Orm\Helper\Date;
use Richbuilds\Orm\Model;
use Richbuilds\Orm\ModelException;

/**
 *
 */

/**
 *
 */
class DatatypeHelperTest extends TestCase
{

    private static Driver|null $Driver;

    /**
     * @return void
     * @throws DriverException
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$Driver = new MySqlDriver(new PDO('mysql:host=127.0.0.1;dbname=orm_test', 'admin', 'x3r0xx3r0x'));
    }

    /**
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$Driver = null;
    }

    /**
     * @dataProvider providerForTestToPHPValueWorks
     */
    public function testToPHPValueWorks(string $field_name, mixed $initial_value, mixed $expected_value, string $expected_error = '')
    {

        $model = (new Model(self::$Driver, 'test_datatypes', [
            'not_null' => ''
        ]));

        if ($expected_error !== '') {
            $this->expectErrorMessage($expected_error);
        }

        $model->set($field_name, $initial_value);

        self::assertEquals($expected_value, $model->get($field_name));

        $model->save();

        self::assertEquals($expected_value, $model->get($field_name));

    }

    /**
     * @return array
     * @throws Exception
     */
    private function providerForTestToPHPValueWorks(): array
    {

        $timestamp = date('Y-m-d H:i:s');
        $timestamp_to_minute = date('Y-m-d H:i:0');

        $datetime = new DateTime($timestamp);

        $datetime_to_minutes = new DateTime($timestamp_to_minute);

        $date = new Date($timestamp);
        $date_as_datetime = (new DateTime($timestamp))->setTime(0,0);

        return [
            ['not_null', null, null, 'cannot be null'],

            ['not_null', '', ''],

            ['bit', 'a', null, 'expected int'],
            ['bit', 1.1, null, 'expected int'],
            ['bit', -0x1, -null, 'cannot be negative'],
            ['bit', 0x11111, null, 'out of range for bit(8)'],

            ['bit', '', null],
            ['bit', null, null],
            ['bit', 0, 0],
            ['bit', 0x1, 0x1],

            ['tinyint', 'a', null, 'expected int'],
            ['tinyint', 1.1, null, 'expected int'],
            ['tinyint', 10000, null, 'out of range'],
            ['tinyint', -10000, null, 'out of range'],


            ['tinyint', null, null],
            ['tinyint', 0, 0],
            ['tinyint', 1, 1],
            ['tinyint', -1, -1],
            ['tinyint', '1', 1],

            ['smallint', 'a', null, 'expected int'],
            ['smallint', 1.1, null, 'expected int'],
            ['smallint', 65535, null, 'out of range'],
            ['smallint', -65535, null, 'out of range'],

            ['smallint', null, null],
            ['smallint', 0, 0],
            ['smallint', 1, 1],
            ['smallint', -1, -1],
            ['smallint', '1', 1],

            ['mediumint', 'a', null, 'expected int'],
            ['mediumint', 1.1, null, 'expected int'],
            ['mediumint', 8388608, null, 'out of range'],
            ['mediumint', -8388609, null, 'out of range'],

            ['mediumint', null, null],
            ['mediumint', 0, 0],
            ['mediumint', 1, 1],
            ['mediumint', -1, -1],
            ['mediumint', '1', 1],

            ['int', 'a', null, 'expected int'],
            ['int', 1.1, null, 'expected int'],
            ['int', 4294967296, null, 'out of range'],
            ['int', -4294967296, null, 'out of range'],

            ['int', null, null],
            ['int', 0, 0],
            ['int', 1, 1],
            ['int', -1, -1],
            ['int', '1', 1],

            ['bigint', 'a', null, 'invalid format'],
            ['bigint', 1.1, null, 'invalid format'],

            ['bigint', null, null],
            ['bigint', 0, '0'],
            ['bigint', 1, '1'],
            ['bigint', -1, '-1'],
            ['bigint', 1, '1'],

            ['decimal', 'a', null, 'invalid format'],
            ['decimal', 100, null, 'invalid format'],
            ['decimal', -100, null, 'invalid format'],

            ['decimal', null, null],
            ['decimal', 0, 0.0],
            ['decimal', 1, 1.0],
            ['decimal', -1, -1.0],
            ['decimal', '1', 1.0],
            ['decimal', 99.99, 99.99],
            ['decimal', -99.99, -99.99],

            ['float', 'a', null, 'invalid format'],

            ['float', null, null],
            ['float', 0, 0.0],
            ['float', 1, 1.0],
            ['float', -1, -1.0],
            ['float', '1', 1.0],
            ['float', 99.99, 99.99],
            ['float', -99.99, -99.99],

            ['char', 'xxxxxxxxx', null, 'too long'],

            ['char', 'xxx', 'xxx'],

            ['varchar', 'xxxxxxxxx', null, 'too long'],

            ['varchar', 'xxx', 'xxx'],

            ['binary', 'xxxxxxxxx', null, 'too long'],

            ['binary', 'xxx', 'xxx'],

            ['varbinary', 'xxxxxxxxx', null, 'too long'],

            ['varbinary', 'xxx', 'xxx'],

            ['tinytext', str_repeat('x', 256), null, 'too long'],

            ['tinytext', 'xxx', 'xxx'],

            ['text', 'xxx', 'xxx'],

            ['mediumtext', 'xxx', 'xxx'],

            ['longtext', 'xxx', 'xxx'],

            ['timestamp', 'a', null, 'invalid format'],
            ['timestamp', 1, null, 'invalid format'],
            ['timestamp', 1.1, null, 'invalid format'],
            ['timestamp', '1900-01-01 00:00:00', null, 'out of range'],

            ['timestamp', null, null],
            ['timestamp', $timestamp, $timestamp],
            ['timestamp', $date, $date->format('Y-m-d 00:00:00')],
            ['timestamp', $datetime, $datetime->format('Y-m-d H:i:s')],

            ['datetime', 'a', null, 'invalid format'],
            ['datetime', 1, null, 'invalid format'],
            ['datetime', 1.1, null, 'invalid format'],

            ['datetime', null, null],
            ['datetime', $timestamp, $datetime],
            ['datetime', $date, $date_as_datetime],
            ['datetime', $datetime, $datetime],
            ['datetime', $datetime->format('Y-m-d\TH:i'), $datetime_to_minutes],

            ['date', 'a', null, 'invalid format'],
            ['date', 1, null, 'invalid format'],
            ['date', 1.1, null, 'invalid format'],

            ['date', null, null],
            ['date', $timestamp, $date],
            ['date', $date, $date],
            ['date', $datetime, $date],

            ['tinyblob', 'xxx', 'xxx'],

            ['blob', 'xxx', 'xxx'],

            ['mediumblob', 'xxx', 'xxx'],

            ['longblob', 'xxx', 'xxx'],

            ['enum', 'x', null, 'value must be one of'],
            ['enum', 'one,two',null, 'value must be one of'],

            ['enum', 'one', 'one'],

            ['set', 'x', null, 'not in set'],
            ['set', 'one,x', null, 'not in set'],

            ['set', 'one', 'one'],
            ['set', 'one,two', 'one,two'],
            ['set', 'two,one', 'two,one'],

            ['geometry', null, null], // TODO:

            ['year', 0, null, 'out of valid year range'],
            ['year', 'a', null, 'invalid format'],

            ['year', 1970, 1970],

            ['bool', 'a', null, 'invalid format'],

            ['bool', 1.1, true],
            ['bool', true, true],
            ['bool', false, false],
            ['bool', 1, true],
            ['bool', 0, false],
            ['bool', -1, true],

            ['unsigned_int', -1, null, 'cannot be negative'],
            ['unsigned_float', -1, null, 'cannot be negative'],
            ['unsigned_decimal', -1, null, 'cannot be negative'],
            ['unsigned_bigint', -1, null, 'cannot be negative'],

        ];
    }

    /**
     * @return void
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function testToGuiType(): void
    {

        $model = new Model(self::$Driver, 'test_datatypes');

        $valid_types = [
            'integer',
            'varchar',
            'float',
            'blob',
            'text',
            'enum',
            'datetime',
            'date',
            'year',
            'set',
            'bool',
            'misc', // TODO
        ];

        foreach($model->getFieldMeta() as $field_name=> $attr) {
            $gui_type = self::$Driver->DatatypeHelper->toGuiType($attr['data_type']);
            self::assertContains($gui_type, $valid_types);
        }

    }

}
