<?php
declare(strict_types=1);

namespace Richbuilds\Tests\Driver;

use Richbuilds\Orm\Driver\DriverException;
use Richbuilds\Orm\Driver\MySql\MySqlQueryBuilder;
use PHPUnit\Framework\TestCase;

/**
 *
 */

/**
 *
 */
class QueryBuilderTest extends TestCase
{
    /**
     * @return void
     * @throws DriverException
     */
    public function testWhereWithConditions(): void
    {

        $qb = new MySqlQueryBuilder();

        $sql = $qb->buildWhere([
            'foo < ' => 1
        ]);

        self::assertEquals(' WHERE `foo` < :foo', $sql);
    }

    /**
     * @return void
     * @throws DriverException
     */
    public function testWhereHandlesIsNull(): void
    {

        $qb = new MySqlQueryBuilder();

        $sql = $qb->buildWhere([
            'foo' => null
        ]);

        self::assertEquals(' WHERE isnull(:foo)', $sql);
    }

    /**
     * @return void
     */
    public function testOrderByWorksForCompositeKeys(): void
    {

        $qb = new MySqlQueryBuilder();

        $sql = $qb->pagination([
            'sort_by'=>'a,b',
            'sort_dir'=>'ASC,DESC'
        ]);

        self::assertStringContainsString('ORDER BY `a` ASC, `b` DESC', $sql);

    }

}
