<?php
declare(strict_types=1);

namespace Richbuilds\Tests\Driver;

use PDO;
use Richbuilds\Orm\Driver\Driver;
use PHPUnit\Framework\TestCase;
use Richbuilds\Orm\Driver\DriverException;
use Richbuilds\Orm\Driver\MySql\MySqlDriver;

/**
 *
 */

/**
 *
 */
class DriverTest extends TestCase
{


    private static Driver|null $Driver;

    /**
     * @return void
     * @throws DriverException
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$Driver = new MySqlDriver(new PDO('mysql:host=127.0.0.1;dbname=orm_test', 'admin', 'x3r0xx3r0x'));
    }

    /**
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$Driver = null;
    }

    /**
     * @return void
     * @throws DriverException
     */
    public function testGetDatabaseNameWorksWithConnection(): void
    {
        self::assertEquals('orm_test', self::$Driver->getDatabaseName());
    }


    /**
     * @return void
     * @throws DriverException
     */
    public function testFetchAllTableNamesWorksWithConnection(): void
    {
        $table_names = self::$Driver->fetchAllTableNames();

        self::assertEquals(
            [
                'categories',
                'comments',
                'countries',
                'locations',
                'menus',
                'posts',
                'posts_tags',
                'tags',
                'test_autocomplete',
                'test_composite_pk',
                'test_datatypes',
                'test_no_auto_pk',
                'test_no_pk',
                'test_truncate',
                'test_uuid_pk',
                'users',
            ],
            $table_names
        );
    }


    /**
     * @return void
     * @throws DriverException
     */
    public function testConnectFailsForUnsupportedDriver(): void
    {

        $this->expectErrorMessage('invalid mysql pdo');

        $driver = new MySqlDriver(new PDO('sqlite:', 'foo', 'bar'));

    }

}
