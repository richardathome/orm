# ORM Documentation

A wireless ORM for MySQL that requires no configuration.

## A Quick Demonstration

```
$driver = new MySqlDriver(new PDO('mysql:host=localhost;dbname=blog', 'username', 'password');

$model = (new Model($driver, 'table name to connect to');

e.g.

$post = (new Model($driver, 'posts');
```
`$posts` can now:

Find a record by its primary key (Orm automatically knows which field / fields are the primary key):

```
$post->fetchByPk(1); 

echo $post->get('title');
```

Find a record by any combination of fields (note this is NOT a fully featured search):

```
$post->fetchFirstBy([
    'title' => "LIKE '%Orm%'",
    'is_visible'=>true
]);

// SELECT * FROM posts WHERE title LIKE 'orm%' AND is_visible = 1
```

`$posts` also knows:

Its parents:
```
$author = $post->fetchParent('author_id');

// SELECT * FROM users WHERE id = 1
```

And its children, including HABTM:

```
echo $post->getChildTableNames(); 
// comments
// tags (via posts_tags table)

$posts = $author->fetchChildren('posts');

// SELECT * FROM posts WHERE posts.author_id = 1

echo coint($posts);

foreach($posts as $post) {
    echo $post->get('title');
}
```

## Real World Examples

Create a multiline order for a new customer:

```
$order = (new Model($driver, 'orders', [
    'user_id'=>[
        'name'=>'John Doe',
        'email'=>'johndoe@madeupmail.com',
        'invoice_address'=>[
            'line1'=>'...',
            'country_id'=1
        ]
    ],
    'order_items'=>[
        [
            'product_id'=1,
            'quantity'=>4
        ],
        [
            'product_id'=2,
            'quantity'=>3
        ]
    ]
]));
```