<?php
declare(strict_types=1);

namespace Richbuilds\Orm;


use Exception;
use Richbuilds\Orm\Driver\Driver;
use Richbuilds\Orm\Driver\DriverException;
use Richbuilds\Orm\Helper\DataTypeException;

/**
 *
 */

/**
 *
 */
class Model
{

    /**
     * @var string|null primary key field name
     */
    private string|null $pk_field_name;

    /**
     * @var array Field values of the model
     */
    private array $values = [];

    /**
     * @var bool true if it's determined the model must be reloaded after save because
     *           mysql might have changed values
     */
    private bool $requires_reload_after_save;

    /**
     * @var array field information keyed by field_name
     */
    private array $field_meta;

    /**
     * @var bool true if the model has a composite primary key
     */
    private bool $has_composite_pk;


    /**
     * Binds the model to the table and optionally sets its starting data
     *
     * @param string $table_name Name of table (plural) this model is bound to
     * @param array $values {optional} Starting values keyed by field name
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function __construct(
        private readonly Driver $Driver,
        private readonly string $table_name,
        array                   $values = []
    )
    {
        $table_meta = $this->Driver->getTableMeta($table_name);

        $this->field_meta = $table_meta['field_info'] ?? [];
        $this->pk_field_name = $table_meta['pk_field_name'] ?? null;

        $this->has_composite_pk = str_contains((string)$this->pk_field_name, ',');

        $this->requires_reload_after_save = $table_meta['requires_reload'] ?? false;

        $this->setValues($values);
    }


    /**
     * Returns an array of each field's metadata keyed by field name
     * or just the info of a specific field if $field_name not null
     *
     * @param string|null $field_name
     *
     * @return array
     *
     * @throws ModelException
     */
    public function getFieldMeta(string|null $field_name = null): array
    {
        if ($field_name === null) {
            return $this->field_meta;
        }

        if (!isset($this->field_meta[$field_name])) {
            throw new ModelException('field ' . $field_name . ' not found in table ' . $this->table_name);
        }

        return $this->field_meta[$field_name];
    }


    /**
     * Returns true if this model has a field called $field_name
     *
     * @param string $field_name
     *
     * @return bool
     */
    private function hasField(string $field_name): bool
    {
        return isset($this->field_meta[$field_name]);
    }


    /**
     * Returns the first model that matches $conditions
     * or false if no matches
     *
     * @param array $conditions
     *
     * @return Model|bool
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function fetchFirstBy(array $conditions = []): self|bool
    {
        $values = $this->Driver->fetchFirstBy(
            $this->table_name,
            $conditions
        );

        if (empty($values)) {
            return false;
        }

        return new self($this->Driver, $this->table_name, $values);
    }


    /**
     * fetches a model by primary key value
     *
     * @param int|string|array $pk_value
     *
     * @return bool|Model
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function fetchByPk(int|string|array $pk_value): bool|self
    {

        if ($this->pk_field_name === null) {
            throw new ModelException('table ' . $this->table_name . ' has no primary key');
        }

        if ($this->has_composite_pk) {

            if (!is_array($pk_value)) {
                throw new ModelException('composite primary key expects array');
            }

            $first = $this->fetchFirstBy($pk_value);
        } else {
            $first = $this->fetchFirstBy([$this->pk_field_name => $pk_value]);
        }

        return $first;

    }


    /**
     * Overwrites all current values with new $values
     *
     * @param array $values [field1_name => value, field2_name => value]
     *
     * @return Model
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function setValues(array $values): self
    {
        $original_values = $this->values;

        $this->values = [];

        foreach ($values as $field_name => $value) {

            try {

                $this->set($field_name, $value);

            } catch (Exception $e) {
                $this->values = $original_values;
                throw $e;
            }
        }

        return $this;
    }


    /**
     * Sets a field value
     *
     * @param string $field_name Which field to set
     * @param mixed $value Value to set field to
     *
     * @return Model
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function set(
        string $field_name,
        mixed  $value
    ): self
    {
        if (
            !$this->hasField($field_name)
            && !is_array($value) // children array
        ) {
            throw new ModelException('unknown field ' . $field_name . ' in table ' . $this->table_name);
        }

        $is_child = $this->hasChild($field_name);
        $is_field = $this->hasField($field_name);

        if (is_array($value)) {

            if ($is_child) {

                foreach ($value as $child_values) {
                    // Can create the child without throwing an error?
                    new self($this->Driver, $field_name, $child_values);
                }

            } elseif ($is_field) {

                $fk_info = $this->field_meta[$field_name]['fk'];

                if (!$fk_info) {
                    throw new ModelException('field ' . $field_name . ' in table ' . $this->table_name . ' - invalid format - unexpected array');
                }

                // can create the child without throwing an error?
                (new self($this->Driver, $fk_info['referenced_table_name']))
                    ->setValues($value);
            } else {

                $habtm_table_name = $this->Driver->getHabtmTableName($this->table_name, $field_name);

                if ($habtm_table_name === false) {
                    throw new ModelException('unknown habtm ' . $field_name . ' in table ' . $this->table_name);
                }

                // TODO: validate habtm
                // parent:
                // this? -> $parent_table_name = $this->getParent($field_name)->setValues($value);
            }

        } else {

            if (get_debug_type($value) === self::class) {

                $expected_type = $this->getFieldMeta($field_name)['fk']['referenced_table_name'];

                if ($value->getTableName() !== $expected_type) {
                    throw new ModelException('field ' . $field_name . ' in table ' . $this->table_name . ' - invalid format - expected ' . $expected_type);
                }

                $value = $value->getPkValue();
            }

            // this is not a child field array or null
            // - convert it to a safe php type and value
            $value = $this->Driver->DatatypeHelper->toPhpValue($value, $this->field_meta[$field_name]);
        }

        $this->values[$field_name] = $value;

        return $this;
    }


    /**
     * Saves the current model and any new children or models its dependent on
     * Rolls back the session if an error occur
     *
     * @return Model
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function save(): self
    {
        // are all required fields populated?
        foreach ($this->field_meta as $field_name => $field_info) {

            if (
                !$field_info['pk']
                && !$field_info['allow_null']
                && $field_info['default_value'] === null
                && $this->get($field_name) === null
            ) {
                throw new ModelException('field ' . $field_name . ' in table ' . $this->table_name . ' is required');
            }

        }

        // attempt to save
        $this->Driver->beginTransaction();

        try {

            $result = $this->_save();
            $this->Driver->commitTransaction();

            return $result;

        } catch (Exception $e) {
            $this->Driver->rollbackTransaction();
            throw $e;
        }

    }


    /**
     * Saves the model recursively
     *
     * @return $this
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    private function _save(): self
    {

        // save any parents this model references
        $this->createParents();

        $values = $this->values;

        // temporarily strip any fields that contain children arrays as they
        // must be saved after this model when we have its pk
        $values = array_filter($values, function ($value) {
            return !is_array($value);
        });

        // convert every PHP value in the values array to its compatible sql type
        foreach ($values as $field_name => $value) {
            $values[$field_name] = $this->Driver->DatatypeHelper->toSQLValue($value, $this->field_meta[$field_name]);
        }

        if ($this->pk_field_name === null) {
            // NOTE: This model has No primary key

            $this->Driver->insert($this->table_name, $values);

        } else {

            // insert or update this model?
            $pk_value = $this->getPkValue();

            if ($pk_value === null) {
                $perform_insert = true;
            } else {
                // we have to check if the record exists because the pk might
                // have been set for a model that hasn't been saved before
                $found = self::fetchByPk($pk_value);
                $perform_insert = $found === false;
            }

            if ($perform_insert) {
                // insert

                if (!$this->has_composite_pk
                    && $this->field_meta[$this->pk_field_name]['column_type'] === 'binary(16)'
                ) {
                    // NOTE: UUID primary key - Assuming field is uuid if its binary(16)

                    // create a uuid
                    $uuid = $this->Driver->fetchUuid();
                    $values[$this->pk_field_name] = $uuid;

                    // save the model
                    $this->Driver->insert($this->table_name, $values);

                    $this->set($this->pk_field_name, $uuid);

                } else {
                    // regular primary key insert

                    $insert_id = $this->Driver->insert($this->table_name, $values);

                    if (!$this->has_composite_pk) {
                        $pk_field_info = $this->getFieldMeta($this->pk_field_name);

                        if ($pk_field_info['auto_inc']) {
                            $this->set($this->pk_field_name, $insert_id);
                        }
                    }
                }

            } else {
                //update

                if (is_array($pk_value)) {
                    $conditions = $pk_value;
                } else {
                    $conditions = [
                        $this->pk_field_name => $this->getPkValue()
                    ];
                }

                $rows_affected = $this->Driver->update(
                    $this->table_name,
                    $values,
                    $conditions
                );

                if ($rows_affected === 0) {
                    // can't make this fail to test it
                    throw new ModelException('no ' . $this->table_name . ' to update for primary key value');
                }
            }

        }

        $this->saveDependants();

        if ($this->requires_reload_after_save) {
            // reload this row to refresh any fields populated by the db
            $pk_value = $this->getPkValue();
            return $this->fetchByPk($pk_value);
        }

        return $this;
    }


    /**
     * Creates any missing dependencies for this model
     * and updates the associated field value with the new foreign keys
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    private function createParents(): void
    {
        foreach ($this->field_meta as $field_name => $field_attr) {
            $parent_values = $this->get($field_name);

            if (is_array($parent_values)) {
                // this field references a new parent - create it
                $parent_table_name = $field_attr['fk']['referenced_table_name'];

                $parent_pk_value = (new self($this->Driver, $parent_table_name, $parent_values))
                    ->_save()
                    ->getPkValue();

                $this->set($field_name, $parent_pk_value);
            }
        }
    }


    /**
     * Save any related children
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    private function saveDependants(): void
    {

        $values = $this->values;

        foreach ($values as $child_table_name => $children) {

            if (!$this->hasField($child_table_name) && is_array($children)) {

                // this model references children that need to be created
                $habtm_table_name = $this->Driver->getHabtmTableName($this->table_name, $child_table_name);

                if (!$habtm_table_name) {
                    // has many $children
                    $parent_pk_value = $this->getPkValue();
                    $parent_field_name = $child_table_name;
                    $this->saveChildren($child_table_name, $children, $parent_pk_value, $parent_field_name);
                } else {
                    // habtm $children through $habtm_table_name
                    $this->saveHabtm($habtm_table_name, $children, $child_table_name);//string $habtm_table_name, array $children, int $parent_pk_value, string $parent_table_name, string $child_table_name
                }

                unset($this->values[$child_table_name]);
            }
        }
    }


    /**
     * Returns an array of all the values
     *
     * @return array
     */
    public function getValues(): array
    {
        $values = $this->values;

        foreach ($this->field_meta as $field_name => $attr) {

            if (!isset($values[$field_name])) {
                $values[$field_name] = null;
            }
        }

        return $values;
    }


    /**
     * Returns a field's value of the correct PHP type
     *
     * @param string $field_name Field name of the value to return
     *
     * @return mixed
     *
     * @throws ModelException
     */
    public function get(string $field_name): mixed
    {
        if (
            !isset($this->values[$field_name]) // might be an unsaved child array
            && !$this->hasField($field_name)
        ) {
            throw new ModelException('unknown field ' . $field_name . ' in table ' . $this->table_name);
        }

        if (!isset($this->values[$field_name])) {
            return null;
        }

        return $this->values[$field_name];
    }


    /**
     * Returns the primary key value or null if not set
     *
     * @return int|string|null|array
     *
     * @throws ModelException
     */
    public function getPkValue(): null|int|string|array
    {

        if ($this->pk_field_name === null) {
            throw new ModelException('table ' . $this->table_name . ' has no primary key');
        }

        if ($this->has_composite_pk) {
            $value = [];

            foreach (explode(',', $this->pk_field_name) as $field_name) {
                $value[$field_name] = $this->get($field_name);
            }

            return $value;
        }

        return $this->get($this->pk_field_name);
    }

    /**
     * Returns the parent model for a foreign key field in this table.
     *
     * @param string $field_name
     *
     * @return $this|null
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function fetchParent(string $field_name = 'parent_id'): self|null
    {
        if (!$this->hasField($field_name)) {
            throw new ModelException('unknown parent ' . $field_name . ' in table ' . $this->table_name);
        }

        $parent_id = $this->get($field_name);

        if ($parent_id === null) {
            return null;
        }

        $fk_info = $this->field_meta[$field_name]['fk'];

        return (new self($this->Driver, $fk_info['referenced_table_name']))
            ->fetchByPk($parent_id);
    }


    /**
     * Returns an array of dependent children
     *
     * @param string $child_table_name Table name of children to return
     *
     * @param array $conditions {optional} Additional conditions to filter by (foreign key back to this model is automatically populated)
     *
     * @return ModelArray Array of related children
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function fetchChildren(
        string $child_table_name,
        array  $conditions = []
    ): ModelArray
    {
        if ($this->hasChild($child_table_name)) {
            $child_model = new self($this->Driver, $child_table_name);

            // where foreign_table.primary_key_value = this models.foreign_key_value
            $child_table_fk_field_name = $child_model->getFkFieldNameByParentTableName($this->table_name);

            $conditions[$child_table_fk_field_name] = $this->getPkValue(); // filter value

            $children = (new Query($this->Driver, $child_table_name))
                ->where($conditions)
                ->fetch();
        } else {
            //habtm through $habtm_table_name
            $habtm_table_name = $this->Driver->getHabtmTableName($this->table_name, $child_table_name);
            $habtm_model = new self($this->Driver, $habtm_table_name);

            $parent_fk_field_name = $habtm_model->getFkFieldNameByParentTableName($this->table_name);
            $child_fk_field_name = $habtm_model->getFkFieldNameByParentTableName($child_table_name);

            $rows = $this->Driver->fetchHabtmChildren($habtm_table_name, $parent_fk_field_name, $child_fk_field_name, $this->getPkValue(), $child_table_name);

            $children = ModelArray::fromRows($this->Driver, $child_table_name, $rows);
        }

        return $children;
    }


    /**
     * Returns the name of the table this model is bound to
     *
     * @return string
     */
    public function getTableName(): string
    {
        return $this->table_name;
    }


    /**
     * Search for $term and returns any matches
     *
     * @param mixed $term
     * @param string $field_name colon separated list of concatenated fields to search. e.g. id:name
     *
     * @return array
     *
     * @throws DriverException
     * @throws ModelException
     */
    public function autocomplete(
        mixed  $term,
        string $field_name = ''
    ): array
    {
        $field_name = $this->getUniqueKeyFieldNames($field_name);
        $field_name = "CONCAT_WS(': ', " . $field_name . ')';

        return $this->Driver->fetchAutocomplete($this->table_name, $this->pk_field_name, $field_name, $term);
    }


    /**
     * Returns this models primary key field name
     *
     * @return null|string
     */
    public function getPkFieldName(): null|string
    {
        return $this->pk_field_name;
    }


    /**
     * Returns an array of tables that are dependent on this model
     *
     * @return array
     * @throws DriverException
     */
    public function getChildTableNames(): array
    {
        return $this->Driver->getChildTableNames($this->table_name);
    }


    /**
     * Returns true if $table_name is a chile of this table
     *
     * @param string $table_name
     *
     * @return bool
     * @throws DriverException
     */
    private function hasChild(string $table_name): bool
    {
        $table_names = $this->getChildTableNames();

        return in_array($table_name, $table_names);
    }


    /**
     * Finds which field points to $parent_table_name
     *
     * @param string $parent_table_name
     *
     * @return string
     *
     * @throws ModelException
     */
    private function getFkFieldNameByParentTableName(string $parent_table_name): string
    {

        foreach ($this->field_meta as $field_name => $attr) {

            if ($attr['fk'] && $attr['fk']['referenced_table_name'] === $parent_table_name) {
                return $field_name;
            }

        }

        // only happens if the schema isn't set up correctly
        throw new ModelException('Model::getFkFieldNameByParentTableName() no field pointing to table ' . $parent_table_name . ' found in table ' . $this->table_name);
    }


    /**
     * @param string $child_table_name
     * @param array $children
     * @param int $pk_value
     * @param string $parent_field_name
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    private function saveChildren(
        string $child_table_name,
        array  $children,
        int    $pk_value,
        string $parent_field_name
    ): void
    {
        // get any child_fields that point to this table
        $child_fk_field_names = $this->Driver->getChildFkFieldNames($child_table_name, $this->table_name, $this->pk_field_name);

        foreach ($children as $child_values) {

            foreach ($child_fk_field_names as $child_field_name) {

                if (!isset($child_values[$child_field_name])) {
                    // child foreign key field is empty - point it to this model
                    $child_values[$child_field_name] = $pk_value;
                }
            }

            (new self($this->Driver, $child_table_name, $child_values))
                ->_save();
        }

        unset($this->values[$parent_field_name]);
    }


    /**
     * @param string $habtm_table_name
     * @param array $children
     * @param string $child_table_name
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    private function saveHabtm(
        string $habtm_table_name,
        array  $children,
        string $child_table_name
    ): void
    {
        // find which fields in the habtm tables point to which tables/fields
        $parent_field_name = null; //'post_id';
        $child_field_name = null; //'tag_id';

        $habtm = new self($this->Driver, $habtm_table_name);

        foreach ($habtm->getFieldMeta() as $field_info) {

            if ($field_info['fk'] !== false) {

                if ($field_info['fk']['referenced_table_name'] == $this->table_name) {
                    $parent_field_name = $field_info['fk']['column_name'];
                } elseif ($field_info['fk']['referenced_table_name'] == $child_table_name) {
                    $child_field_name = $field_info['fk']['column_name'];
                }
            }

            if ($parent_field_name !== null && $child_field_name !== null) {
                // we've found the fields - we can stop looking
                break;
            }

        }

        if ($parent_field_name === null || $child_field_name === null) {
            // NOTE: This should only happen if the schema is invalid
            throw new ModelException('wtf?!!!');
        }

        foreach ($children as $child) {

            if (get_debug_type($child) !== Model::class) {
                $child = (new self($this->Driver, $child_table_name, $child))
                    ->_save();
            }

            (new self($this->Driver, $habtm_table_name, [
                $parent_field_name => $this->getPkValue(),
                $child_field_name => $child->getPkValue()
            ]))->_save();
        }

    }


    /**
     * Returns a unique string description for this model
     *
     * @param string $separator
     * @return string
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function getUniqueName(string $separator = ' < '): string
    {
        $field_name = $this->getUniqueKeyFieldNames();

        if (!str_contains($field_name, ',')) {
            return (string)$this->get($field_name);
        }

        // unique name is a combination of fields ($field_name is comma separated)

        $unique_name = '';

        foreach (array_reverse(explode(',', $field_name)) as $sub_field_name) {

            if ($this->field_meta[$sub_field_name]['fk'] !== false) {
                $parent = $this->fetchParent($sub_field_name);

                if ($parent !== null) {
                    $unique_name .= $parent->getUniqueName($separator) . $separator;
                }

            } else {
                $value = $this->get($sub_field_name);
                $unique_name .= $this->Driver->DatatypeHelper->toString($value) . $separator;
            }

        }

        return substr($unique_name, 0, -strlen($separator));
    }


    /**
     * returns a comma separated list of fields used in the first unique index on the table
     *
     * @param string $field_name
     *
     * @return string
     *
     * @throws DriverException
     * @throws ModelException
     */
    private function getUniqueKeyFieldNames(string $field_name = ''): string
    {

        if ($field_name === '') {
            // determine a unique key to search on (first unique index)

            foreach ($this->getIndexInfo() as $attr) {

                if ($attr['CONSTRAINT_TYPE'] === 'UNIQUE') {
                    $field_name = $attr['columns'];
                    break;
                }

            }
        }

        if ($field_name === '') {

            if ($this->pk_field_name === null) {
                throw new ModelException('primary key or no unique key set for table ' . $this->table_name);
            }

            $field_name = $this->pk_field_name;

        }

        return $field_name;
    }


    /**
     * Fetches index metadata for this table
     *
     * @return array
     * @throws DriverException
     */
    private function getIndexInfo(): array
    {
        return $this->Driver->getIndexInfo($this->table_name);
    }


    /**
     * Validates $values against the model schema
     *
     * @param array $values
     *
     * @return array array of errors keyed by field name or empty if no errors
     * @throws ModelException
     */
    public function validate(array $values): array
    {
        $orig_values = $this->values;

        $errors = [];

        foreach ($this->getFieldMeta() as $field_name => $attr) {

            $value = $values[$field_name] ?? null;

            try {

                $this->set($field_name, $value);

            } catch (Exception $e) {
                $errors[$field_name] = $e->getMessage();
            }

        }

        $this->values = $orig_values;

        return $errors;
    }


    /**
     * @return Model
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function delete(): self
    {
        if ($this->getPkValue() === null) {
            throw new ModelException('no primary key set - cannot delete');
        }

        $this->Driver->beginTransaction();

        $deleted = self::_delete();

        $this->Driver->commitTransaction();

        return $deleted;
    }


    /**
     * @return Model
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    private function _delete(): self
    {

        if ($this->has_composite_pk) {
            $rows_affected = $this->Driver->delete($this->table_name, $this->getPkValue());
        } else {
            $rows_affected = $this->Driver->delete($this->table_name, [
                $this->pk_field_name => $this->getPkValue()
            ]);
        }

        if ($rows_affected !== 1) {
            // this should never happen
            throw new ModelException($rows_affected . ' rows affected');
        }

        $this->values[$this->pk_field_name] = null;

        return new self($this->Driver, $this->table_name);
    }


    /**
     * Performs a 'safe' (all in one transaction) cascading delete which
     * deletes any children before deleting the model
     *
     * @return Model
     *
     * @throws ModelException
     */
    public function cascadingDelete(): self
    {

        try {

            $this->Driver->beginTransaction();
            $deleted = $this->_cascadingDelete();
            $this->Driver->commitTransaction();

            return $deleted;

        } catch (Exception $e) {
            $this->Driver->rollbackTransaction();
            throw new ModelException('failed to cascade delete: ' . $e->getMessage());
        }
    }


    /**
     * Recursively deletes the model and any dependencies
     *
     * @return Model
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    private function _cascadingDelete(): self
    {
        foreach ($this->getChildTableNames() as $child_table) {

            foreach ($this->fetchChildren($child_table) as $child) {
                $child->_cascadingDelete();
                unset($this->values[$child_table]);
            }
        }

        $this->_delete();

        return $this;
    }


}