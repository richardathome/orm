<?php
declare(strict_types=1);

namespace Richbuilds\Orm;

use ArrayIterator;
use Richbuilds\Orm\Driver\Driver;
use Richbuilds\Orm\Driver\DriverException;
use Richbuilds\Orm\Helper\DataTypeException;

/**
 *
 */

/**
 *
 */
class ModelArray extends ArrayIterator
{

    /**
     * @param Model ...$models
     */
    public function __construct(Model ...$models)
    {
        parent::__construct($models);
    }


    /**
     * Builds a ModelArray of $table_name from an array of database $rows
     *
     * @param Driver $driver
     * @param string $table_name
     * @param array $rows
     *
     * @return ModelArray
     *
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public static function fromRows(Driver $driver, string $table_name, array $rows): self
    {
        $model_array = new self();

        foreach ($rows as $values) {
            $model_array[] = new Model($driver, $table_name, $values);
        }

        return $model_array;
    }


    /**
     * @return Model|null
     */
    public function current(): Model|null
    {
        return parent::current();
    }


    /**
     * @param mixed $key
     *
     * @return Model
     */
    public function offsetGet(mixed $key): Model
    {
        return parent::offsetGet($key);
    }


    /**
     * @return void
     *
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function save(): void
    {
        /**
         * @var Model $model
         */
        foreach (parent::getArrayCopy() as $id => $model) {
            parent::offsetSet($id, $model->save());
        }
    }
}