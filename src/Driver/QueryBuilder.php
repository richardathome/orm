<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Driver;

use Richbuilds\Orm\Model;

/**
 *
 */

/**
 *
 */
abstract class QueryBuilder
{
    protected string $quote = '`'; // character used to quote table/field names

    /**
     * Returns the SQL required to fetch field information about
     * table :table_name
     *
     * @return string
     *
     * @throws DriverException
     */
    abstract public function fetchFieldInfoForTable(): string;


    /**
     * Returns the SQL required to insert $values into $table_name
     *
     * @param string $table_name
     * @param array $values
     *
     * @return string
     */
    abstract public function insert(string $table_name, array $values): string;


    /**
     * Returns the SQL required to update $values of $table_name which match
     * $conditions
     *
     * @param string $table_name
     * @param array $values
     * @param array $conditions
     *
     * @return string
     * @throws DriverException
     */
    abstract public function update(string $table_name, array $values, array $conditions): string;


    /**
     * Returns the SQL required to fetch back the Foreign Key information
     * for :table_name
     *
     * @param string $database_name
     *
     * @return string
     */
    abstract public function buildFetchFkInfoForTable(string $database_name): string;


    /**
     * Returns the SQL required to perform a
     * SELECT * FROM $table_name WHERE $conditions with $pagination
     *
     * @param string $table_name
     * @param array $conditions
     * @param array $pagination
     *
     * @return string
     *
     * @throws DriverException
     */
    abstract public function buildPaginatedSelect(
        string $table_name,
        array  $conditions = [],
        array  $pagination = []
    ): string;

    /**
     * Returns a where clause built from $conditions
     *
     * @param array $conditions
     *
     * @return string
     *
     * @throws DriverException
     */
    public function buildWhere(array $conditions = []): string
    {
        if (empty($conditions)) {
            return '';
        }

        $quote = $this->quote;

        $where = ' WHERE ';

        foreach ($conditions as $field_name => $value) {

            if (str_contains($field_name, ' ')) {
                // field_name LIKE, field_name >, etc.

                // extract the comparator and trim it from the field name
                list($field_name, $comparator) = explode(' ', $field_name);

                if (!in_array($comparator, [
                    '>', '>=', '=', '<=', '<', 'LIKE'
                ])) {
                    throw new DriverException('invalid comparator ' . $comparator);
                }
            } else {
                // default comparator
                $comparator = '=';
            }

            if ($value === null) {

                $where .= 'isnull(:' . $field_name . ')';

            } else {

                $where .= $quote . $field_name . $quote . ' ' . $comparator;

                $where .= ' :' . str_replace('.', '_', $field_name);

            }

            $where .= ' AND ';
        }

        return substr($where, 0, -5); // strip the trailing ' AND '
    }


    /**
     * Returns the SQL fragment required to paginate a select statement
     *
     * @param array $options
     *
     * @return string
     */
    abstract public function pagination(array $options = []): string;


    /**
     * Returns the SQL required to return first matching $table_name which
     * matches $conditions
     *
     * @param string $table_name
     * @param array $conditions
     *
     * @return string
     * @throws DriverException
     */
    public function buildFetchFirstBy(string $table_name, array $conditions = []): string
    {
        $quote = $this->quote;

        $sql = 'SELECT * FROM ' . $quote . $table_name . $quote;
        $sql .= self::buildWhere($conditions);

        $sql .= ' LIMIT 1';

        return $sql;
    }

    /**
     * Returns the SQL required ot fetch the Index information about :table_name
     *
     * @param string $database_name
     * @return string
     */
    abstract public function buildFetchIndexesForTable(string $database_name): string;


    /**
     * Returns the SQL required to fetch the names of all tables in the database
     *
     * @param string $database_name
     * @return string
     *
     */
    abstract public function fetchAllTableNames(string $database_name): string;

    /**
     * Returns the SQL required to perform an autocomplete query on $table_name
     *
     * @param string $table_name
     * @param string $pk_field_name
     * @param string $field_to_search
     * @param int $limit
     *
     * @return string
     */
    abstract public function buildFetchAutoComplete(
        string $table_name,
        string $pk_field_name,
        string $field_to_search,
        int    $limit = 20
    ): string;


    /**
     * Returns the SQL required to fetch the child table names
     * for :parent_table_name
     *
     * @param string $database_name
     * @return string
     */
    abstract public function buildFetchChildTableNames(string $database_name): string;


    /**
     * Returns the SQL required to fetch field names that point
     * to :parent_table_name
     *
     * @param string $database_name
     * @return string
     */
    abstract public function fetchChildFkFieldNames(string $database_name): string;


    /**
     * Returns the SQL required to fetch the HABTM table which
     * links :parent_table_name and :child_table_name
     * @param string $database_name
     * @return string
     */
    abstract public function fetchHabtmTableName(string $database_name): string;


    /**
     * @param string $table_name
     * @param array $conditions
     *
     * @return string
     *
     * @throws DriverException
     */
    abstract public function buildDelete(string $table_name, array $conditions): string;


    /**
     * Trims everything after and including a space after a field name
     *
     * e.g. ['name LIKE' => 'foo'] becomes ['name' => 'foo']
     *
     * Replaces . with _
     *
     * e.g ['abc.def' => 'foo'] becomes ['abc_def' => 'foo']
     *
     * @param array $values
     *
     * @return array
     */
    public function trimFieldNames(array $values): array
    {
        foreach ($values as $field_name => $value) {
            $stripped_field_name = str_replace('.', '_', $field_name);

            if (str_contains($stripped_field_name, ' ')) {
                $stripped_field_name = substr($stripped_field_name, 0, strpos($stripped_field_name, ' '));
            }

            if ($stripped_field_name !== $field_name) {
                // replace the old field with the new field
                unset($values[$field_name]);
                $values[$stripped_field_name] = $value;
            }
        }

        return $values;
    }


    /**
     * Returns the SQL necessary to get a new UUID from the DB
     *
     * @return string
     */
    public function buildFetchUuid(): string
    {
        return 'SELECT UUID_TO_BIN(UUID())';
    }

    /**
     * Returns the SQL required to truncate a table
     */
    abstract public function truncate(string $database_name, string $table_name): string;


    /**
     * Returns the query to fetch the children rows from a HABTM
     *
     * @param string $habtm_table_name
     * @param string $parent_fk_field_name
     * @param Model $child_model
     * @param string $child_fk_field_name
     *
     * @return string
     */
    abstract public function buildFetchHabtmChildren(
        string $habtm_table_name,
        string $parent_fk_field_name,
        Model  $child_model,
        string $child_fk_field_name
    ): string;

    /**
     * Returns the SQL required to fetch the database name from the database
     *
     * @return string
     */
    abstract public function fetchDatabaseName(): string;

}