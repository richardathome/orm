<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Driver\MySql;

use Richbuilds\Orm\Driver\QueryBuilder;
use Richbuilds\Orm\Model;

/**
 *
 */

/**
 *
 */
class MySqlQueryBuilder extends QueryBuilder
{


    /**
     * {@inheritDoc}
     */
    public function fetchDatabaseName(): string
    {
        return <<<SQL
SELECT DATABASE();
SQL;

    }


    /**
     * {@inheritDoc}
     */
    public function fetchFieldInfoForTable(): string
    {
        return <<<SQL
SELECT
       INFORMATION_SCHEMA.COLUMNS.*
FROM
     INFORMATION_SCHEMA.COLUMNS
  WHERE 
        TABLE_SCHEMA = :database_name
    AND TABLE_NAME = :table_name
ORDER BY 
         ORDINAL_POSITION
SQL;
    }


    /**
     * {@inheritDoc}
     */
    public function fetchChildFkFieldNames(string $database_name): string
    {
        return <<<SQL
SELECT 
    COLUMN_NAME
FROM
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
    TABLE_SCHEMA = '$database_name'
        AND TABLE_NAME = :child_table_name
        AND REFERENCED_TABLE_NAME = :parent_table_name
        AND REFERENCED_COLUMN_NAME = :parent_pk_field_name
SQL;
    }

    /**
     * {@inheritDoc}
     */
    public function truncate(string $database_name, string $table_name): string
    {
        return 'TRUNCATE TABLE `' . $database_name . '`.`' . $table_name . '`';
    }


    /**
     * {@inheritDoc}
     */
    public function fetchHabtmTableName(string $database_name): string
    {
        return <<<SQL
SELECT 
    TABLE_NAME
FROM
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
    TABLE_SCHEMA = '$database_name'
        AND REFERENCED_TABLE_NAME = :parent_table_name
        OR REFERENCED_TABLE_NAME = :child_table_name
GROUP BY table_name
HAVING COUNT(TABLE_NAME) >= 2
SQL;
    }


    /**
     * {@inheritDoc}
     */
    public function fetchAllTableNames(string $database_name): string
    {

        return <<<SQL
SELECT 
       TABLE_NAME 
FROM 
     information_schema.tables
WHERE 
      TABLE_SCHEMA = '$database_name'
      AND TABLE_TYPE='BASE TABLE'
ORDER BY TABLE_NAME
SQL;

    }


    /**
     * {@inheritDoc}
     */
    public function pagination(array $options = []): string
    {

        if (empty($options)) {
            return '';
        }

        $quote = $this->quote;

        $sort_by = $options['sort_by'] ?? 'id'; // TODO: id might not always be a good choice
        $sort_dir = $options['sort_dir'] ?? 'asc';

        $sql = ' ORDER BY ';

        if (!str_contains($sort_by, ',')) {
            $sql .= $quote . $sort_by . $quote . ' ' . $sort_dir . ' ';
        } else {
            // order by is over multiple fields
            $sort_by = explode(',', $sort_by);
            $sort_dir = explode(',', $sort_dir);

            foreach ($sort_by as $i => $field_name) {
                $sql .= $quote . $field_name . $quote . ' ';
                $sql .= $sort_dir[$i] ?? 'ASC';

                $sql .= ', ';
            }

            $sql = substr($sql, 0, -2) . ' ';
        }


        $limit = $options['limit'] ?? 20;
        $page = $options['page'] ?? 1;

        if ($limit > 0) {
            $sql .= 'LIMIT ' . ($page - 1) * $limit . ',' . $limit;
        }

        return $sql;
    }


    /**
     * {@inheritDoc}
     */
    public function insert(string $table_name, array $values): string
    {
        $quote = $this->quote;
        $sql = 'INSERT INTO ' . $quote . $table_name . $quote;

        if (empty($values)) {
            return $sql . ' DEFAULT VALUES';
        }

        $sql .= ' (';

        foreach ($values as $field_name => $value) {
            $sql .= $quote . $field_name . $quote . ', ';
        }

        $sql = substr($sql, 0, -2);
        $sql .= ') VALUES (';

        foreach ($values as $field_name => $value) {
            $sql .= ':' . $field_name . ', ';
        }

        $sql = substr($sql, 0, -2);
        $sql .= ')';

        return $sql;
    }


    /**
     * {@inheritDoc}
     */
    public function update(string $table_name, array $values, array $conditions): string
    {
        $quote = $this->quote;

        $sql = 'UPDATE ' . $quote . $table_name . $quote . ' SET ';

        foreach (array_keys($values) as $field_name) {
            $sql .= $quote . $field_name . $quote . ' = :' . $field_name . ',';
        }

        $sql = substr($sql, 0, -1);

        $sql .= $this->buildWhere($conditions);

        return $sql;
    }


    /**
     * {@inheritDoc}
     */
    public function buildFetchFkInfoForTable(string $database_name): string
    {

        return <<<SQL
SELECT 
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE.*
FROM
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
    TABLE_SCHEMA = '$database_name'
        AND TABLE_NAME = :table_name
        AND KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME IS NOT NULL
SQL;
    }


    /**
     * {@inheritDoc}
     */
    public function buildPaginatedSelect(
        string $table_name,
        array  $conditions = [],
        array  $pagination = []
    ): string
    {
        $quote = $this->quote;

        $sql = 'SELECT ' . $quote . $table_name . $quote . '.* FROM ' . $quote . $table_name . $quote;

        $sql .= $this->buildWhere($conditions);

        $sql .= $this->pagination($pagination);

        return $sql;
    }


    /**
     * {@inheritDoc}
     */
    public function buildFetchIndexesForTable(string $database_name): string
    {

        return <<<SQL
SELECT 
    stat.table_schema AS database_name,
    stat.table_name,
    stat.index_name,
    GROUP_CONCAT(stat.column_name
        ORDER BY stat.seq_in_index
        SEPARATOR ',') AS columns,
    tco.constraint_type
FROM
    information_schema.statistics stat
        JOIN
    information_schema.table_constraints tco ON stat.table_schema = tco.table_schema
        AND stat.table_name = tco.table_name
        AND stat.index_name = tco.constraint_name
WHERE
    stat.non_unique = 0
        AND stat.table_schema NOT IN ('information_schema' , 'sys', 'performance_schema', 'mysql')
        AND stat.table_schema = '$database_name'
        AND stat.table_name = :table_name
GROUP BY stat.table_schema , stat.table_name , stat.index_name , tco.constraint_type
ORDER BY stat.table_schema , stat.table_name

SQL;

    }


    /**
     * {@inheritDoc}
     */
    public function buildFetchAutoComplete(
        string $table_name,
        string $pk_field_name,
        string $field_to_search,
        int    $limit = 20
    ): string
    {

        return <<<SQL
SELECT 
       `$pk_field_name` as id,
       $field_to_search as label
FROM 
    `$table_name`
WHERE
    $field_to_search LIKE :term
LIMIT $limit
SQL;

    }


    /**
     * {@inheritDoc}
     */
    public function buildFetchChildTableNames(string $database_name): string
    {

        return <<<SQL
SELECT 
    TABLE_NAME
FROM
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
    REFERENCED_TABLE_SCHEMA = '$database_name'
  AND TABLE_NAME <> :parent_table_name1
  AND REFERENCED_TABLE_NAME = :parent_table_name2
SQL;

    }


    /**
     * {@inheritDoc}
     */
    public function buildDelete(string $table_name, array $conditions): string
    {
        $quote = $this->quote;

        $sql = 'DELETE FROM ' . $quote . $table_name . $quote;
        $sql .= self::buildWhere($conditions);

        return $sql;

    }


    /**
     * {@inheritDoc}
     */
    public function buildFetchHabtmChildren(
        string $habtm_table_name,
        string $parent_fk_field_name,
        Model  $child_model,
        string $child_fk_field_name
    ): string
    {

        $child_pk_field_name = $child_model->getPkFieldName();
        $child_table_name = $child_model->getTableName();

        return <<<SQL
SELECT 
    `$child_table_name`.*
FROM
    `$child_table_name`
        INNER JOIN
    `$habtm_table_name` ON `$habtm_table_name`.`$parent_fk_field_name` = :parent_value
        AND `$habtm_table_name`.`$child_fk_field_name` = `$child_table_name`.`$child_pk_field_name`
SQL;
    }

}