<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Driver\MySql;

use Exception;
use Richbuilds\Orm\Helper\DataTypeException;
use Richbuilds\Orm\Helper\DatatypeHelper;

/**
 *
 */

/**
 *
 */
class MysqlDataTypeHelper extends DatatypeHelper
{

    /**
     * {@inheritDoc}
     */
    public function _toPhpValue(mixed $value, array $field_info): mixed
    {

        $type = $field_info['data_type'];
        $is_signed = $field_info['signed'];
        $column_type = $field_info['column_type'];
        $max_length = $field_info['max_length'];

        try {
            return match ($type) {
                'bit' => $this->toBit($value, $is_signed, $column_type),
                'tinyint' => $this->toTinyInt($value, $is_signed),
                'smallint' => $this->toSmallInt($value, $is_signed),
                'mediumint' => $this->toMediumInt($value, $is_signed),
                'int' => $this->toInt($value, $is_signed),
                'bigint' => $this->toBigInt($value, $is_signed),
                'decimal' => $this->toDecimal($value, $field_info),
                'float' => $this->toFloat($value, $field_info),
                'char', 'varchar', 'binary', 'varbinary',
                'tinytext', 'text', 'mediumtext', 'longtext' => $this->toChar($value, $max_length),
                'timestamp' => $this->toTimeStamp($value),
                'datetime' => $this->toDatetime($value),
                'date' => $this->toDate($value),
                'tinyblob', 'blob', 'mediumblob', 'longblob' => $this->toBlob($value, $max_length),
                'enum' => $this->toEnum($value, $column_type),
                'geometry' => $this->toGeometry($value, $field_info),
                'year' => $this->toYear($value),
                'set' => $this->toSet($value, $column_type),
                '_bool' => $this->toBool($value),
                default => throw new DataTypeException('unhandled type: ' . $type),
            };
        } catch (Exception $e) {
            throw new DataTypeException('field ' . $field_info['name'] . ' in table ' . $field_info['table'] . ' - ' . $e->getMessage() . ' : ' . self::toString($value), $e->getCode(), $e);
        }

    }

    /**
     * {@inheritDoc}
     */
    protected function toBlob(mixed $value, int $max_length): string
    {
        return $this->toChar($value, $max_length);
    }


    /**
     * {@inheritDoc}
     */
    public function toGuiType(string $data_type): string
    {

        return match ($data_type) {
            'int', 'tinyint', 'smallint', 'mediumint', 'bigint' => 'integer',
            'decimal', 'float' => 'float',
            'varchar', 'char' => 'varchar',
            'tinytext', 'text', 'mediumtext', 'longtext' => 'text',
            'tinyblob', 'blob', 'mediumblob', 'longblob' => 'blob',
            'enum'=>'enum',
            'set'=>'set',
            'datetime', 'timestamp'=>'datetime',
            'date'=>'date',
            'year'=>'year',
            '_bool'=>'bool',
            'bit', 'binary', 'varbinary', 'geometry' => 'misc',
            default => throw new DataTypeException('unhandled type ' . $data_type),
        };
    }
}