<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Driver\MySql;

use PDO;
use Richbuilds\Orm\Driver\Driver;
use Richbuilds\Orm\Driver\DriverException;

/**
 *
 */

/**
 *
 */
class MySqlDriver extends Driver
{
    /**
     * @param PDO $pdo
     * @throws DriverException
     */
    public function __construct(PDO $pdo)
    {

        if ($pdo->getAttribute(PDO::ATTR_DRIVER_NAME) !== 'mysql') {
            throw new DriverException('invalid mysql pdo');
        }

        $query_builder = new MySqlQueryBuilder();
        $data_type_helper = new MysqlDataTypeHelper($this);

        parent::__construct($pdo, $query_builder, $data_type_helper);

    }


    /**
     * {@inheritDoc}
     */
    protected function fetchDatabaseName(): string
    {
        return $this->fetchValue($this->QueryBuilder->fetchDatabaseName());
    }


    /**
     * {@inheritDoc}
     */
    public function fetchAllTableNames(): array
    {
        $cache_key = 'getAllTableNames.';

        $table_names = $this->Cache->get($cache_key);

        if ($table_names === null) {
            $table_names = $this->fetchAll($this->QueryBuilder->fetchAllTableNames($this->getDatabaseName()));
            $table_names = array_column($table_names, 'TABLE_NAME');
            $this->Cache->set($cache_key, $table_names);
        }

        return $table_names;
    }


    /**
     * {@inheritDoc}
     */
    public function getTableMeta(string $table_name): array
    {

        $cache_key = 'getFieldInfoForTable.' . $table_name;

        $table_info = $this->Cache->get($cache_key);

        if ($table_info === null) {

            $result = $this->fetchAll(
                $this->QueryBuilder->fetchFieldInfoForTable(),
                [
                    'database_name'=>$this->getDatabaseName(),
                    'table_name' => $table_name
                ]
            );

            if (empty($result)) {
                throw new DriverException('table ' . $table_name . ' not found');
            }

            $fk_info = $this->getFkInfo($table_name);
            $pk_field_name = null;
            $requires_reload = false;

            $field_info = [];

            foreach ($result as $row) {
                $field_name = $row['COLUMN_NAME'];
                $data_type = $row['DATA_TYPE'];
                $column_type = $row['COLUMN_TYPE'];
                $is_pk = $row['COLUMN_KEY'] === 'PRI';
                $is_unique = $row['COLUMN_KEY'] === 'UNI';
                $max_length = $row['CHARACTER_MAXIMUM_LENGTH'];
                $allow_null = $row['IS_NULLABLE'] === 'YES';
                $default_value = $row['COLUMN_DEFAULT'];
                $fk = $fk_info[$field_name] ?? false;
                $unsigned = str_contains($column_type, 'unsigned');
                $comment = $row['COLUMN_COMMENT'];
                $is_auto_inc = $row['EXTRA'] === 'auto_increment';

                $options = [];

                if (preg_match('/enum\((.*)\)/', $column_type, $matches)) {
                    $options = explode(',', $matches[1]);
                }

                if (preg_match('/set\((.*)\)/', $column_type, $matches)) {
                    $options = explode(',', $matches[1]);
                }

                if ($column_type === 'tinyint(1)') {
                    $data_type = '_bool';
                }

                $field_info[$field_name] = [
                    'table' => $table_name,
                    'name' => $field_name,
                    'data_type' => $data_type,
                    'column_type' => $column_type,
                    'pk' => $is_pk,
                    'is_unique' => $is_unique,
                    'max_length' => $max_length,
                    'allow_null' => $allow_null,
                    'signed' => !$unsigned,
                    'default_value' => $default_value,
                    'fk' => $fk,
                    'comment' => $comment,
                    'options' => $options,
                    'auto_inc' => $is_auto_inc
                ];


                $field_info[$field_name]['gui_type'] = $this->DatatypeHelper->toGuiType($data_type);

                if ($is_pk) {
                    $pk_field_name .= $field_name . ',';
                }

                // TODO: there might be other indicators the row needs
                // reloading after a save?
                // check for insert/update triggers
                if (
                    $default_value !== null
                    || ($is_pk && !$is_auto_inc)
                ) {
                    $requires_reload = true;
                }

            }

            if ($pk_field_name !== null) {
                $pk_field_name = substr($pk_field_name, 0, -1);
            }

            $table_info = [
                'field_info' => $field_info,
                'pk_field_name' => $pk_field_name,
                'requires_reload' => $requires_reload
            ];

            $this->Cache->set($cache_key, $table_info);
        }

        return $table_info;
    }


    /**
     * {@inheritDoc}
     */
    public function getFkInfo(string $table_name): array
    {
        $cache_key = 'getFkInfoForTable.' . $table_name;

        $info = $this->Cache->get($cache_key);

        if ($info === null) {
            $info = [];

            $results = $this->fetchAll(
                $this->QueryBuilder->buildFetchFkInfoForTable($this->getDatabaseName()),
                [
                    'table_name' => $table_name
                ]
            );

            foreach ($results as $row) {

                $attr = [
                    'referenced_table_name' => $row['REFERENCED_TABLE_NAME'],
                    'referenced_column_name' => $row['REFERENCED_COLUMN_NAME'],
                    'column_name' => $row['COLUMN_NAME']
                ];

                $info[$row['COLUMN_NAME']] = $attr;
            }

            $this->Cache->set($cache_key, $info);
        }

        return $info;
    }

    /**
     * {@inheritDoc}
     */
    public function getChildFkFieldNames(
        string $child_table_name,
        string $parent_table_name,
        string $parent_pk_field_name
    ): array
    {
        $cache_key = 'getChildFkFieldNames.' . $child_table_name . '.' . $parent_table_name . '.' . $parent_pk_field_name;

        $field_names = $this->Cache->get($cache_key);

        if ($field_names === null) {

            $sql = $this->QueryBuilder->fetchChildFkFieldNames($this->getDatabaseName());

            $result = $this->fetchAll($sql, [
                'child_table_name' => $child_table_name,
                'parent_table_name' => $parent_table_name,
                'parent_pk_field_name' => $parent_pk_field_name
            ]);

            $field_names = array_column($result, 'COLUMN_NAME');

            $this->Cache->set($cache_key, $field_names);
        }

        return $field_names;
    }


    /**
     * {@inheritDoc}
     */
    public function getChildTableNames(string $parent_table_name): array
    {
        $cache_key = 'getChildTableNames.' . $parent_table_name;

        $table_names = $this->Cache->get($cache_key);

        if ($table_names === null) {

            $rows = $this->fetchAll(
                $this->QueryBuilder->buildFetchChildTableNames($this->getDatabaseName()),
                [
                    'parent_table_name1' => $parent_table_name,
                    'parent_table_name2' => $parent_table_name
                ]
            );

            $table_names = array_column($rows, 'TABLE_NAME');
            $this->Cache->set($cache_key, $table_names);
        }

        return $table_names;
    }



}