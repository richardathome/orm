<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Driver;

use DateTime;
use Exception;
use PDO;
use PDOStatement;
use Richbuilds\Orm\Helper\Cache;
use Richbuilds\Orm\Helper\DataTypeException;
use Richbuilds\Orm\Helper\DatatypeHelper;
use Richbuilds\Orm\Model;
use Richbuilds\Orm\ModelException;

/**
 *
 */

/**
 * Interfaces with the database via pdo
 */
abstract class Driver
{

    protected string $database_name;

    protected Cache $Cache;

    public const DATE_FORMAT_SQL = 'Y-m-d';
    public const DATE_TIME_FORMAT_SQL = 'Y-m-d H:i:s';

    /**
     * @param PDO $pdo
     * @param QueryBuilder $QueryBuilder
     * @param DatatypeHelper $DatatypeHelper
     */
    public function __construct(
        protected PDO          $pdo,
        protected QueryBuilder $QueryBuilder,
        public DatatypeHelper  $DatatypeHelper)
    {
        $this->Cache = new Cache();
    }


    /**
     * @return string
     *
     * @throws DriverException
     */
    public function getDatabaseName(): string
    {
        $cache_key = 'getDatabaseName';

        $database_name = $this->Cache->get($cache_key);

        if ($database_name === null) {

            $database_name = $this->fetchDatabaseName();
            $this->Cache->set($cache_key, $database_name);
        }

        return $database_name;
    }


    /**
     * @return string
     * @throws DriverException
     */
    abstract protected function fetchDatabaseName(): string;


    /**
     * Prepares and executes $sql with $values
     *
     * @param string $sql SQL to execute
     * @param array $values Values to pass into the execute statement
     *
     * @return PDOStatement
     *
     * @throws DriverException
     */
    private function prepareAndExec(string $sql, array $values = []): PDOStatement
    {
        try {
            $start_time = new DateTime();

            $values = $this->QueryBuilder->trimFieldNames($values);

            $stmt = $this->pdo->prepare($sql);

            /*
             * Parameters are bound this way instead of $stmt->execute($values)
             * because pdo mistakenly converts bit fields to a string behind
             * the scenes and causes the sql statement to fail!
             */
            foreach ($values as $field_name => $value) {

                if (get_debug_type($value) === DateTime::class) {
                    $value = $value->format(self::DATE_TIME_FORMAT_SQL);
                }

                $pdo_type = match (get_debug_type($value)) {
                    'int', 'bit' => PDO::PARAM_INT,
                    default => PDO::PARAM_STR,
                };


                $stmt->bindValue($field_name, $value, $pdo_type);
            }

            $stmt->execute();

            /**
             * if (AutoOrm::logging()) {
             * $end_time = new DateTime();
             * $duration = $end_time->diff($start_time);
             * Log::add($sql, $values, $duration);
             * }
             */

            return $stmt;
        } catch (Exception $e) {
            throw new DriverException($e->getMessage() . "\n" . $sql . "\n" . print_r($values, true), (int)$e->getCode(), $e);
        }
    }


    /**
     * Executes $sql with $params and returns all the results
     *
     * @param string $sql
     * @param array $params
     *
     * @return array
     * @throws DriverException
     */
    public function fetchAll(string $sql, array $params = []): array
    {
        {
            $stmt = $this->prepareAndExec($sql, $params);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
    }


    /**
     * Returns the first row of $table_name that matches $conditions
     *
     * @param string $table_name
     * @param array $conditions
     *
     * @return array
     *
     * @throws DriverException
     */
    public function fetchFirstBy(string $table_name, array $conditions = []): array
    {

        $sql = $this->QueryBuilder->buildFetchFirstBy($table_name, $conditions);

        $stmt = $this->prepareAndExec($sql, $conditions);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result === false) {
            return [];
        }

        return $result;

    }


    /**
     * Inserts $values into $table_name
     *
     * @param string $table_name Table to insert into
     * @param array $values Values to insert keyed by field_name
     *
     * @return int|string PK of row inserted
     * @throws DriverException
     */
    public function insert(string $table_name, array $values): int|string
    {
        $sql = $this->QueryBuilder->insert($table_name, $values);

        $this->prepareAndExec($sql, $values);

        return $this->pdo->lastInsertId();

    }


    /**
     * Updates $table with $values filtered by $conditions
     *
     * @param string $table_name Table to update
     * @param array $values Values to update table with
     * @param array $conditions Which record(s) to update
     *
     * @return int number of rows affected
     *
     * @throws DriverException
     */
    public function update(string $table_name, array $values, array $conditions): int
    {
        $sql = $this->QueryBuilder->update($table_name, $values, $conditions);

        $stmt = $this->prepareAndExec($sql, array_merge($values, $conditions));

        return $stmt->rowCount();
    }


    /**
     * Starts a transaction
     *
     * @throws DriverException
     */
    public function beginTransaction(): void
    {
        if ($this->pdo->inTransaction()) {
            throw new DriverException('transaction already started');
        }

        $this->pdo->beginTransaction();
    }

    /**
     * Commits the current transaction
     *
     * @throws DriverException
     */
    public function commitTransaction(): void
    {
        if (!$this->pdo->inTransaction()) {
            throw new DriverException('attempt to commit transaction when transaction not started');
        }

        $this->pdo->commit();
    }

    /**
     * Cancels the current transaction - ignores any errors
     */
    public function rollbackTransaction(): void
    {

        try {

            if ($this->pdo->inTransaction()) {
                $this->pdo->rollBack();
            }

        } catch (Exception) {
            // ignore the error - pdo() probably failed due to no connection
        }

    }


    /**
     * Returns the results of autocomplete query
     *
     * @param string $table_name table to search
     * @param string $pk_field_name primary key of search table to return
     * @param string $field field name in search table to return
     * @param mixed $term search term
     *
     * @return array [][1 => 'matching term']
     * @throws DriverException
     */
    public function fetchAutocomplete(string $table_name, string $pk_field_name, string $field, mixed $term): array
    {
        $sql = $this->QueryBuilder->buildFetchAutoComplete($table_name, $pk_field_name, $field);

        $stmt = $this->prepareAndExec($sql, ['term' => $term . '%']);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * @param $habtm_table_name
     * @param string $parent_fk_field_name
     * @param string $child_fk_field_name
     * @param int $parent_pk_value
     * @param string $child_table_name
     *
     * @return array
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function fetchHabtmChildren($habtm_table_name, string $parent_fk_field_name, string $child_fk_field_name, int $parent_pk_value, string $child_table_name): array
    {
        $child_model = new Model($this, $child_table_name);

        $sql = $this->QueryBuilder->buildFetchHabtmChildren($habtm_table_name, $parent_fk_field_name, $child_model, $child_fk_field_name);

        return $this->prepareAndExec($sql, [
            'parent_value' => $parent_pk_value
        ])
            ->fetchAll(PDO::FETCH_ASSOC);

    }

    /**
     * @param string $table_name
     * @param array $conditions
     *
     * @return int rows affected
     *
     * @throws DriverException
     */
    public function delete(string $table_name, array $conditions): int
    {
        $sql = $this->QueryBuilder->buildDelete($table_name, $conditions);
        $stmt = $this->prepareAndExec($sql, $conditions);

        return $stmt->rowCount();
    }


    /**
     * @param string $sql
     * @return mixed
     * @throws DriverException
     */
    public function fetchValue(string $sql): mixed
    {
        $stmt = $this->prepareAndExec($sql);
        $row = $stmt->fetch();

        return $row[0];
    }

    /**
     * Returns an array of all the names of the tables in the database
     *
     * @return array
     * @throws DriverException
     */
    abstract public function fetchAllTableNames(): array;

    /**
     * Returns index information for a table
     *
     * @param string $table_name
     *
     * @return array
     * @throws DriverException
     */
    public function getIndexInfo(string $table_name): array
    {

        $cache_key = 'getIndexInfoForTable.' . $table_name;

        $index_info = $this->Cache->get($cache_key);

        if ($index_info === null) {

            $index_info = $this->fetchAll(
                $this->QueryBuilder->buildFetchIndexesForTable($this->getDatabaseName()),
                [
                    'table_name' => $table_name
                ]
            );

            $this->Cache->set($cache_key, $index_info);
        }

        return $index_info;
    }

    /**
     * Returns a regex  pattern that validates that a php value fits the MySQL decimal(n,m) type
     *
     * @param array $field_info
     *
     * @return string
     */
    public function getDecimalRegexPattern(array $field_info): string
    {

        // extract the decimal info from the mysql type
        preg_match('/decimal\(([0-9]+),+([0-9]+)\)/', $field_info['column_type'], $matches);

        // build the matching regex validation pattern
        $pattern = '\-?\d{0,' . (int)$matches[1] - (int)$matches[2] . '}';

        if (isset($matches[2])) {
            $pattern .= '(\.\d{1,' . (int)$matches[2] . '})';
        }

        $pattern .= '?';

        return '/^' . $pattern . '$/';
    }


    /**
     * Returns information about a tables children
     *
     * @param string $table_name Table name to return information about
     *
     * @return array
     *
     * @throws DriverException
     *
     */
    abstract public function getFkInfo(string $table_name): array;


    /**
     * Returns metadata about the fields in a table
     *
     * @param string $table_name Table to interrogate
     *
     * @return array Keyed by field_name
     *
     * @throws DriverException
     * @throws DataTypeException
     */
    abstract public function getTableMeta(string $table_name): array;


    /**
     * Returns a list of child tables for $parent_table_name
     *
     * @param string $parent_table_name
     *
     * @return array
     * @throws DriverException
     */
    abstract public function getChildTableNames(string $parent_table_name): array;


    /**
     * @param string $child_table_name
     * @param string $parent_table_name
     * @param string $parent_pk_field_name
     *
     * @return array
     *
     * @throws DriverException
     */
    abstract public function getChildFkFieldNames(
        string $child_table_name,
        string $parent_table_name,
        string $parent_pk_field_name
    ): array;

    /**
     * Returns the name of the table that is the habtm join table for
     * $parent_table_name and $child_table_name or false if there is none
     *
     * @param string $parent_table_name
     * @param string $child_table_name
     *
     * @return string|bool
     *
     * @throws DriverException
     */
    public function getHabtmTableName(
        string $parent_table_name,
        string $child_table_name
    ): string|bool
    {
        $cache_key = 'getHabtmTableName.' . $parent_table_name . '.' . $child_table_name;

        $table_names = $this->Cache->get($cache_key);

        if ($table_names === null) {

            $sql = $this->QueryBuilder->fetchHabtmTableName($this->getDatabaseName());

            $table_names = $this->fetchAll($sql, [
                'parent_table_name' => $parent_table_name,
                'child_table_name' => $child_table_name
            ]);

            if (empty($table_names)) {
                // not a habtm child relationship
                return false;
            }

            if (count($table_names) !== 1) {
                // NOTE: this should never happen! ;-)
                throw new DriverException('OrmHelper::getHabtmTableName: Found more than one table that could be habtm for table ' . $parent_table_name . ' to ' . $child_table_name);
            }

            $table_names = $table_names[0]['TABLE_NAME'];

            $this->Cache->set($cache_key, $table_names);
        }

        return $table_names;
    }

    /**
     * Fetches a new UUID from the DB
     *
     * @return string
     * @throws DriverException
     */
    public function fetchUuid(): string
    {
        return $this->fetchValue($this->QueryBuilder->buildFetchUuid());
    }

    /**
     * @param string $table_name
     *
     * @return void
     * @throws DriverException
     */
    public function truncate(string $table_name): void
    {
        // truncate implies an explicit commit
        $this->prepareAndExec($this->QueryBuilder->truncate($this->getDatabaseName(), $table_name));
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder(): QueryBuilder
    {
        return $this->QueryBuilder;
    }

    /**
     * Returns the inTransaction state of the pdo connection
     * - used mainly for testing purposes
     *
     * @return bool
     */
    public function inTransaction(): bool
    {
        return $this->pdo->inTransaction();
    }
}