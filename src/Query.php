<?php
declare(strict_types=1);

namespace Richbuilds\Orm;

use Richbuilds\Orm\Driver\Driver;
use Richbuilds\Orm\Driver\DriverException;
use Richbuilds\Orm\Helper\DataTypeException;

/**
 *
 */

/**
 *
 */
class Query
{

    /**
     * @var string table this query is bound to
     */
    private string $table_name;

    /**
     * @var array query where conditions
     */
    private array $conditions = [];

    /**
     * @var array query pagination options
     */
    private array $pagination = [];

    /**
     * @var string|null user specified sql for query
     */
    private null|string $sql = null;


    /**
     * Creates a query bound to $table_name
     *
     * @param Driver $Driver
     * @param string $table_name
     *
     * @throws DataTypeException
     * @throws DriverException
     * @throws ModelException
     */
    public function __construct(
        private readonly Driver $Driver,
        string                  $table_name
    )
    {
        $this->table_name = $table_name;

        $model = new Model($this->Driver, $table_name);

        // default to sort by primary key field ASC
        $this->sortBy($model->getPkFieldName(), 'ASC')
            ->setPage(1)
            ->limit(10);
    }


    /**
     * Sets the where clause for the query
     *
     * e.g.
     *      SELECT * FROM posts WHERE id < 10
     *      $query = Query::from('posts')->where(['id <' => 10]);
     *
     * @param array $conditions
     *
     * @return Query
     */
    public function where(array $conditions): self
    {
        $this->conditions = $conditions;

        return $this;
    }


    /**
     * Sets the query sort field and direction
     *
     * e.g.
     *
     *      SELECT * FROM posts ORDER BY title asc
     *      $query = Query::from('posts')->sortBy('title', 'asc');
     *
     * @param string $sort_by
     * @param string $sort_dir
     * @return $this
     */
    public function sortBy(string $sort_by, string $sort_dir): self
    {
        $this->pagination['sort_by'] = $sort_by;
        $this->pagination['sort_dir'] = $sort_dir;

        return $this;
    }


    /**
     * Sets the limit (maximum number of records returned) for the Query
     *
     * e.g.
     *
     *      SELECT * FROM posts LIMIT 10
     *      $posts = Query::from('posts')->limit(10);
     *
     * @param int $limit
     * @return $this
     */
    public function limit(int $limit): self
    {
        $this->pagination['limit'] = $limit;

        return $this;
    }


    /**
     * Performs the query and returns the results as a ModelsArray
     *
     * e.g.
     *
     *      $posts = Query::from('posts')->fetch();
     *
     * @return ModelArray
     *
     * @throws DriverException
     * @throws DataTypeException
     * @throws ModelException
     */
    public function fetch(): ModelArray
    {
        $rows = $this->Driver->fetchAll(
                $this->getSql(),
                $this->conditions
            );

        return ModelArray::fromRows($this->Driver, $this->table_name, $rows);
    }


    /**
     * Returns the table name this query is bound to
     *
     * @return string
     */
    public function getTableName(): string
    {
        return $this->table_name;
    }


    /**
     * Sets which page of a paginated result (defined by limit per page)
     *
     * e.g.
     *      SELECT * FROM posts LIMIT 20,20
     *      $posts = Query::from('posts')->page(2)->limit(20);
     *
     * @param int $page
     *
     * @return $this
     */
    public function setPage(int $page): self
    {

        $this->pagination['page'] = $page;

        return $this;
    }

    /**
     * Sets the custom SQL used to fetch the models
     *
     * @param string $sql
     * @param array $params
     *
     * @return $this
     */
    public function setSql(string $sql, array $params = []): self
    {

        $sql = trim($sql);

        if (!str_starts_with('select ', strtolower($sql))) {
            $sql = 'SELECT * FROM ' . $this->table_name . ' ' . $sql;
        }

        $this->sql = $sql;
        $this->conditions = $params;

        return $this;
    }

    /**
     * @return string
     *
     * @throws DriverException
     */
    public function getSql(): string
    {
        if ($this->sql === null) {

            return $this->Driver->getQueryBuilder()->buildPaginatedSelect($this->table_name, $this->conditions, $this->pagination);
        }

        return $this->sql;
    }

}