<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Helper;

use DateTime;
use DateTimeZone;
use Exception;

/**
 *
 */

/**
 *
 */
class Date
{

    private DateTime $datetime;

    /**
     * @param string $format
     * @param string $datetime
     * @param DateTimeZone|null $timezone
     *
     * @return DateTime|false
     * @throws Exception
     */
    public static function createFromFormat(string $format, string $datetime, DateTimeZone $timezone = null): bool|self
    {

        $datetime = DateTime::createFromFormat($format, $datetime, $timezone);

        if ($datetime === false) {
            return false;
        }

        return new self($datetime->format('Y-m-d'));

    }


    /**
     * @param string|null $datetime
     * @throws Exception
     */
    public function __construct(string $datetime = null)
    {

        $datetime = new DateTime($datetime);

        $datetime->setTime(0,0);

        $this->datetime = $datetime;
    }


    /**
     * @param string $format
     *
     * @return string
     */
    public function format(string $format): string
    {
        return $this->datetime->format($format);
    }

}