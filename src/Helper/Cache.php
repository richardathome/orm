<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Helper;

/**
 *
 */

/**
 * Simple in request memory cache
 */
class Cache
{

    private static array $cache = [];

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key): mixed
    {
        return self::$cache[$key] ?? null;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function set(string $key, mixed $value): static
    {
        self::$cache[$key] = $value;

        return $this;
    }

}