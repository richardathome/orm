<?php
declare(strict_types=1);

namespace Richbuilds\Orm\Helper;

use DateTime;
use Exception;
use Richbuilds\Orm\Driver\Driver;

/**
 *
 */

/**
 *
 */
abstract class DatatypeHelper
{

    /**
     * @param Driver $Driver
     */
    public function __construct(
        private readonly Driver $Driver
    )
    {
    }


    /**
     * Returns a string describing the suggested type of control to use as an input/display
     *
     * Used by AutoGui to build forms etc.
     *
     * @param string $data_type
     *
     * @return string
     *
     * @throws DataTypeException
     */
    abstract public function toGuiType(string $data_type): string;

    /**
     * Coverts (mixed)$value to closest/safest PHP datatype for internal storage
     *
     * @param mixed $value
     * @param array $field_info
     *
     * @return mixed
     *
     * @throws DataTypeException
     */
    public function toPhpValue(mixed $value, array $field_info): mixed
    {

        if (
            $value === ''
            && ($field_info['allow_null'] || $field_info['auto_inc'])
        ) {
            $value = null;
        }

        if ($value === null) {

            if (
                !$field_info['allow_null']
                && ($field_info['default_value'] === null && !$field_info['auto_inc'])
            ) {
                throw new DataTypeException('cannot be null');
            }

            return null;
        }

        return $this->_toPhpValue($value, $field_info);

    }


    /**
     * @param mixed $value
     * @param array $field_info
     *
     * @throws DataTypeException
     *
     * @return mixed
     */
    abstract public function _toPhpValue(mixed $value, array $field_info): mixed;

    /**
     * Validates $value is either DateTime or a string that looks like a datetime
     * and returns $value as DateTime
     *
     * @param mixed $value
     *
     * @return DateTime
     *
     * @throws DataTypeException
     */
    protected function toDatetime(mixed $value): DateTime
    {

        $value = self::toString($value);

        // HTML Format? 2022-05-11T13:01
        preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}$/', $value, $matches);

        if (count($matches)) {
            $value = DateTime::createFromFormat('Y-m-d\TH:i', $value)
                ->format('Y-m-d H:i:s');
        }

        if ($this->isDate($value)) {
            $datetime = new DateTime($value);
            $value = $datetime->format('Y-m-d H:i:s');
        }

        $value = DateTime::createFromFormat($this->Driver::DATE_TIME_FORMAT_SQL, $value);

        if ($value === false) {
            throw new DataTypeException('invalid format');
        }

        return $value;
    }


    /**
     * Validate $value is an int and returns $value cast to int
     *
     * @param mixed $value
     * @param bool $is_signed
     *
     * @return int
     *
     * @throws DataTypeException
     */
    protected function toInt(mixed $value, bool $is_signed): int
    {

        if (get_debug_type($value) == 'bool') {
            $value = $value ? 1 : 0;
        }

        $value = (int)self::validateInt($value, $is_signed);

        if (($is_signed && ($value < -2147483648 || $value > 2147483647))
            || (!$is_signed && ($value < 0 || $value > 4294967295))
        ) {
            throw new DataTypeException('out of range for int');
        }

        return $value;
    }


    /**
     * Converts $value to a tinyint or bool
     *
     * @param int|string|bool $value
     * @param bool $is_signed
     *
     * @return int
     *
     * @throws DataTypeException
     */
    protected function toTinyInt(mixed $value, bool $is_signed): int
    {

        // if it cannot be converted to an int, it can't be converted to a tinyint
        $value = self::toInt($value, $is_signed);

        // TODO: get the bit size from $column_type to determine min/max value
        if (
            (!$is_signed && ($value < 0 || $value > 255))
            || ($is_signed && ($value < -128 || $value > 127))
        ) {
            throw new DataTypeException('out of range for tinyint');
        }

        return $value;
    }

    /**
     * @param mixed $value
     *
     * @return bool
     *
     * @throws DataTypeException
     */
    protected function toBool(mixed $value): bool
    {

        if (get_debug_type($value) === 'string' && $value !== '0' && $value !== '1') {
            throw new DataTypeException('invalid format');
        }

        if (get_debug_type($value) === 'bool') {
            return (bool)$value;
        }

        // return true if value is non-zero
        return $value != 0;

    }


    /**
     * @param int|string $value
     * @param bool $is_signed
     * @param string $column_type
     * @return int
     *
     * @throws DataTypeException
     */
    protected function toBit(mixed $value, bool $is_signed, string $column_type): int
    {

        $value = self::toInt($value, $is_signed);

        if ($value < 0) {
            throw new DataTypeException('cannot be negative');
        }

        preg_match('/bit\(([0-9]+)\)/', $column_type, $matches);

        $bits = (int)$matches[1];
        $max_value = pow(2, $bits);

        if ($value > $max_value) {
            throw new DataTypeException('out of range for ' . $column_type);
        }

        return $value;
    }

    /**
     * @param int|string $value
     * @param bool $is_signed
     * @return int
     * @throws DataTypeException
     */
    protected function toSmallInt(mixed $value, bool $is_signed): int
    {
        $value = self::toInt($value, $is_signed);

        if (
            (!$is_signed && ($value < 0 || $value > 65535))
            || ($is_signed && ($value < -32768 || $value > 32767))
        ) {
            throw new DataTypeException('out of range for smallint');
        }

        return $value;
    }


    /**
     * @param int|string $value
     * @param bool $is_signed
     * @return int
     * @throws DataTypeException
     */
    protected function toMediumInt(mixed $value, bool $is_signed): int
    {
        $value = (int)self::validateInt($value, $is_signed);

        if (
            (!$is_signed && ($value < 0 || $value > 16777215))
            || ($is_signed && ($value < -8388608 || $value > 8388607))
        ) {
            throw new DataTypeException('out of range for mediumint');
        }

        return $value;
    }


    /**
     * Validates $value looks like a bit int and converts it to a string
     *
     * @param string|int $value
     * @param bool $is_signed
     *
     * @return string
     *
     * @throws DataTypeException
     */
    protected function toBigInt(mixed $value, bool $is_signed): string
    {

        $value = self::toString($value);

        if (str_starts_with($value, '-') && !$is_signed) {
            throw new DataTypeException('cannot be negative');
        }

        if (!preg_match('/^-?[0-9]+$/', $value)) {
            throw new DataTypeException('invalid format');
        }

        return $value;
    }


    /**
     * @param string|float|int $value
     * @param array $field_info
     *
     * @return float
     * @throws DataTypeException
     */
    protected function toDecimal(mixed $value, array $field_info): float
    {

        $value = self::toString($value);

        $pattern = $this->Driver->getDecimalRegexPattern($field_info);

        if (!preg_match($pattern, $value)) {
            throw new DataTypeException('invalid format');
        }

        if (str_starts_with($value, '-') && !$field_info['signed']) {
            throw new DataTypeException('cannot be negative');
        }

        return (float)$value;
    }


    /**
     * @param string|float $value
     * @param array $field_info
     *
     * @return float
     * @throws DataTypeException
     */
    protected function toFloat(mixed $value, array $field_info): float
    {

        $value = self::toString($value);

        if (!preg_match('/(\d+(?:\.\d+)?)/', $value)) {
            throw new DataTypeException('invalid format');
        }

        if (str_starts_with($value, '-') && !$field_info['signed']) {
            throw new DataTypeException('cannot be negative');
        }

        return (float)$value;
    }


    /**
     * Validates $value and converts to a string
     *
     * @param mixed $value
     * @param int $max_length
     * @return string
     * @throws DataTypeException
     */
    protected function toChar(mixed $value, int $max_length): string
    {

        $value = self::toString($value);

        if (strlen($value) > $max_length) {
            throw new DataTypeException('value too long');
        }

        return $value;
    }


    /**
     * @param string $value
     * @param int $max_length
     * @return string
     * @throws DataTypeException
     */
    abstract protected function toBlob(mixed $value, int $max_length): string;


    /**
     * @param int|string $value
     * @param string $column_type
     * @return string
     * @throws DataTypeException
     */
    protected function toEnum(mixed $value, string $column_type): string
    {

        $value = self::toString($value);

        $options = self::extractOptions('enum', $column_type);

        if (!in_array($value, $options)) {
            throw new DataTypeException('value must be one of: ' . $column_type);
        }

        return $value;
    }

    /**
     * @param mixed $value
     *
     * @return Date
     *
     * @throws DataTypeException
     * @throws Exception
     */
    protected function toDate(mixed $value): Date
    {

        switch (get_debug_type($value)) {

            case DateTime::class:
            case Date::class:

                $value = $value->format('Y-m-d');
                break;

            default:

                $value = self::toString($value);

                $datetime = DateTime::createFromFormat('Y-m-d H:i:s', $value);

                if ($datetime === false) {
                    $datetime = DateTime::createFromFormat('Y-m-d', $value);
                }

                if ($datetime === false) {
                    throw new DataTypeException('invalid format');
                }

                $value = $datetime->format('Y-m-d');

        }

        return Date::createFromFormat($this->Driver::DATE_FORMAT_SQL, $value);
    }


    /**
     * Validates $value and converts to string
     *
     * @param mixed $value
     *
     * @return string
     *
     * @throws DataTypeException
     */
    protected function toTimeStamp(mixed $value): string
    {

        $value = match (get_debug_type($value)) {
            DateTime::class, Date::class => $value->format('Y-m-d H:i:s'),
            default => self::toString($value),
        };

        if (!preg_match('/^[12][0-9]{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[01]) ([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/', $value)) {
            throw new DataTypeException('invalid format');
        }

        // split date string into valid parts for testing
        preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/', $value, $matches);

        if ($matches[1] < 1970) {
            throw new DataTypeException('out of range');
        }

        return $value;

    }

    /**
     * @param mixed $value
     * @param array $field_info
     *
     * @return string
     */
    protected function toGeometry(mixed $value, array $field_info): string
    {
        // TODO: needs implementing - probably with a custom Geometry Datatype
        return self::toString($value);
    }


    /**
     * @param mixed $value
     * @return int
     *
     * @throws DataTypeException
     */
    protected function toYear(mixed $value): int
    {
        $value = self::toInt($value, false);

        if ($value < 1901 || $value > 2155) {
            throw new DataTypeException('out of valid year range');
        }

        return $value;
    }


    /**
     * @param mixed $value
     * @param string $column_type
     *
     * @return string
     *
     * @throws DataTypeException
     */
    protected function toSet(mixed $value, string $column_type): string
    {
        $value = self::toString($value);

        $options = self::extractOptions('set', $column_type);

        foreach (explode(',', $value) as $sub_value) {

            if (!in_array($sub_value, $options)) {
                throw new DataTypeException('not in set: ' . $column_type);
            }

        }

        return $value;
    }


    /**
     * Converts a PHP type to an SQL insert/update type
     *
     * @param mixed $value
     * @param array $field_info
     *
     * @return int|float|string|null
     */
    public function toSQLValue(mixed $value, array $field_info): null|int|float|string
    {
        if ($value === null) {
            return null;
        }

        return match ($field_info['data_type']) {
            'bigint', 'datetime', 'date' => self::toString($value),
            '_bool' => $value ? 1 : 0,
            default => $value
        };
    }


    /**
     * Validates $value looks like an int and returns it
     *
     * @param string $value
     * @param bool $is_signed
     *
     * @return mixed either an int or string representation of a large int
     *
     * @throws DataTypeException
     */
    private function validateInt(mixed $value, bool $is_signed): mixed
    {
        if (!preg_match('/^-?[0-9]+$/', (string)$value)) {
            throw new DataTypeException('invalid format - expected int');
        }

        if (str_starts_with((string)$value, '-') && !$is_signed) {
            throw new DataTypeException('cannot be negative');
        }

        return $value;

    }


    /**
     * Extracts the options from an enum or set column_type
     *
     * @param string $type
     * @param string $column_type
     *
     * @return array
     */
    private function extractOptions(string $type, string $column_type): array
    {
        preg_match('/^' . $type . '\((.*)\)$/', $column_type, $matches);

        $options = str_replace("'", '', $matches[1]);

        return explode(',', $options);

    }


    /**
     * Converts $value to a string representation
     *
     * @param mixed $value
     *
     * @return string
     */
    public function toString(mixed $value): string
    {
        return match (get_debug_type($value)) {
            DateTime::class => $value->format('Y-m-d H:i:s'),
            Date::class => $value->format('Y-m-d'),
            default => (string)$value,
        };
    }


    /**
     * @param mixed $value
     *
     * @return bool
     */
    protected function isDate(mixed $value): bool
    {
        $value = $this->toString($value);

        if (preg_match('/^\d{4}-\d{2}-\d{2} [0-2][0-3]:[0-5][0-9]:[0-5][0-9]$/', $value)) {
            return true;
        }

        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $value)) {
            return true;
        }

        return false;

    }

}